# List of SDK dependencies of this app, this information is also included in an encrypted form in the APK.
# For more information visit: https://d.android.com/r/tools/dependency-metadata

library {
  maven_library {
    groupId: "com.google.firebase"
    artifactId: "firebase-core"
    version: "11.0.4"
  }
  digests {
    sha256: "|\210\363\250P\227\274 \234O\202\257\231\337o\270\235%\264\213o\202\'#\000\374\307!\351\212U\344"
  }
}
library {
  maven_library {
    groupId: "com.google.firebase"
    artifactId: "firebase-analytics"
    version: "11.0.4"
  }
  digests {
    sha256: "A\363\301\225\302\321\354\327\355\024\316\v\302\225N{\360\vQ\352\367L\351\340\217`\327<\036M\234R"
  }
}
library {
  maven_library {
    groupId: "com.google.firebase"
    artifactId: "firebase-analytics-impl"
    version: "11.0.4"
  }
  digests {
    sha256: "\262&(\323AR\311\264\270/\3062\240|si\207\320\235\326f\325\311\371!\352\361Y\347\346\000\v"
  }
}
library {
  maven_library {
    groupId: "com.google.android.gms"
    artifactId: "play-services-basement"
    version: "11.0.4"
  }
  digests {
    sha256: "\031\370j\023\n\366\275(\234\216$\207:3N\236\357\306\337\\\341>\004\207\331Iu\326\305\217o\003"
  }
}
library {
  maven_library {
    groupId: "com.google.firebase"
    artifactId: "firebase-common"
    version: "11.0.4"
  }
  digests {
    sha256: "\3236\016e\331\350U\272I\272\232\254\200D\f*\017~\354%\276\003-j-HC\307]f\201\353"
  }
}
library {
  maven_library {
    groupId: "com.google.firebase"
    artifactId: "firebase-iid"
    version: "11.0.4"
  }
  digests {
    sha256: "\260\314p\310\356\247\244\036X\215\006\034\203Y&\277\004q\254\253\224\\w\327\021Y\n\211U\375\244)"
  }
}
library {
  maven_library {
    groupId: "com.google.android.gms"
    artifactId: "play-services-tasks"
    version: "11.0.4"
  }
  digests {
    sha256: "\215\377\3307`\367\004\231\362k\377\272sR\331\346X\224G^\303*\302\255\241\275\203,\314\201\v%"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "support-v4"
    version: "26.1.0"
  }
  digests {
    sha256: "6\3308]\341\276w\221#\032\313\223;uq\230\371|\265;\307\320F\350\304\274@=!L\252\312"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "support-compat"
    version: "26.1.0"
  }
  digests {
    sha256: "}m\240\034\371vk\027\005\306\310\f\374\022\'J\211[@lL(y\000\260zV\024\\\246\3000"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "support-media-compat"
    version: "26.1.0"
  }
  digests {
    sha256: "\235\214\356|\324\016\377\"\353\336\271\f\216p\365\356\226\305\275%\313,>;9@\342r\205\243\351\212"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "support-core-utils"
    version: "26.1.0"
  }
  digests {
    sha256: "O\332mN\2640\227\036;\035\255tV\230\2033\363t\260\364\272\025\371\2309\312\032\n\265\025\\\212"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "support-core-ui"
    version: "26.1.0"
  }
  digests {
    sha256: "\202\3658\005\025\2313^\250\201\354&D\aT|\253R\276u\017\026\316\t\234\373\'uO\307U\377"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "support-fragment"
    version: "26.1.0"
  }
  digests {
    sha256: "\240\2533i\357@\376\031\221`i/\004c\245\366?\022w\353\373d\335X|v\375\261(\327k2"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "support-annotations"
    version: "27.1.0"
  }
  digests {
    sha256: "3e\226\002\006\303\322\260\236\204_U^\177\210\370\357\374\215/\000\263i\346lK\343\204\002\222\231\317"
  }
}
library {
  maven_library {
    groupId: "android.arch.lifecycle"
    artifactId: "runtime"
    version: "1.0.0"
  }
  digests {
    sha256: "\344\343N]\002\275\020.\2159\335\274)\371\352\330\241Za\343g\231=\002#\201\226\254HP\232\330"
  }
}
library {
  maven_library {
    groupId: "android.arch.lifecycle"
    artifactId: "common"
    version: "1.0.0"
  }
  digests {
    sha256: "\206\2770\032 \255\f\320\243\221\342*R\346\373\371\005u\300\226\377\203#?\251\375\rR\263!\221!"
  }
}
library {
  maven_library {
    groupId: "android.arch.core"
    artifactId: "common"
    version: "1.0.0"
  }
  digests {
    sha256: "Q\222\223L\327=\363.,\025r.\327\374H\215\336\220\272\256\311\256\003\000\020\335\032\200\373Nt\341"
  }
}
library {
  maven_library {
    groupId: "com.android.support.constraint"
    artifactId: "constraint-layout"
    version: "1.0.2"
  }
  digests {
    sha256: "\260\306\210\314+qr`\217\201S\246\211\327F\332@\367\036R\327\342\376+\375\235\362\371-\267p\205"
  }
}
library {
  maven_library {
    groupId: "com.android.support.constraint"
    artifactId: "constraint-layout-solver"
    version: "1.0.2"
  }
  digests {
    sha256: "\214bRZ\233\305\317\365c:\226\313\2332\377\376\314\257A\270\204\032\250\177\302&\a\a\r\352\233\215"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "appcompat-v7"
    version: "22.2.0"
  }
  digests {
    sha256: "-Xgi\204\020\264\037u\024\f\221\326\301\345\215\247J\340\371{\257n\v\335\037|\301\001|\353,"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "recyclerview-v7"
    version: "22.2.0"
  }
  digests {
    sha256: ":\215\241E\205\372\034\201\360n|\357M\223\247d\037\003#\330\371\204\377\232{\327\246\344\026\264h\210"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "cardview-v7"
    version: "22.2.0"
  }
  digests {
    sha256: "\025\2714\366\237\020\330_Z\334\022#\033\241\267\ba\212}\257nQ\002\257B\255\337,g\250^,"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "design"
    version: "22.2.0"
  }
  digests {
    sha256: "A\377_p\r\033\370\301\220\341\323\2543X/\222\255n\037;\020\200\310\270\342\254O{p;\347j"
  }
}
library {
  maven_library {
    groupId: "com.google.firebase"
    artifactId: "firebase-messaging"
    version: "11.0.4"
  }
  digests {
    sha256: "~~\245\2745[\206\360\035\003\332\265\357\367s\357\v\270\360\370\000\204P\322\250\262\n\374I\206\274\331"
  }
}
library {
  maven_library {
    groupId: "com.google.android.gms"
    artifactId: "play-services-ads"
    version: "11.0.4"
  }
  digests {
    sha256: "u,\355\374\n\226\356\360\337\216\306|x\026BQ6a\264\320\306x\273\333\232C\340I]\346\275\356"
  }
}
library {
  maven_library {
    groupId: "com.google.android.gms"
    artifactId: "play-services-ads-lite"
    version: "11.0.4"
  }
  digests {
    sha256: "\347[0\330\331\337\000\213\272\352\036\005\264\220\224\356,L5MIP\375S\004\215_#D\331\342\255"
  }
}
library {
  maven_library {
    groupId: "com.google.android.gms"
    artifactId: "play-services-base"
    version: "11.0.4"
  }
  digests {
    sha256: "\320\004r\031\257[,\276O7M\355O\005\rj\n7M\360\260\376\221^\2601\316\227\225\314\353\342"
  }
}
library {
  maven_library {
    groupId: "com.google.android.gms"
    artifactId: "play-services-clearcut"
    version: "11.0.4"
  }
  digests {
    sha256: "\347\274\334\364\345\321\267e\231n_C\347n\306\276Z\023\255\347\257\016\215@Y\377M(\247\035*\271"
  }
}
library {
  maven_library {
    groupId: "com.google.android.gms"
    artifactId: "play-services-gass"
    version: "11.0.4"
  }
  digests {
    sha256: "\333\216LJ\004,\213\217\253\237\'\245\357B\220Az:\321\252;\227[\241\2035\234|X\266\256\376"
  }
}
library {
  maven_library {
    groupId: "com.google.android.gms"
    artifactId: "play-services-safetynet"
    version: "11.0.4"
  }
  digests {
    sha256: "Y\320 \032\035o\246F`\335\035m\313>`\021;J\357&S\247\000D\027\356]x\313\345Q|"
  }
}
library {
  maven_library {
    groupId: "com.google.android.gms"
    artifactId: "play-services-location"
    version: "11.0.4"
  }
  digests {
    sha256: "\252o\304`\2403U\213\356\207\273\016\331\371\373\313\004\266\265\024\024\335\252\205\251\221\245\224\373A\206F"
  }
}
library {
  maven_library {
    groupId: "com.squareup.okhttp"
    artifactId: "okhttp"
    version: "2.4.0"
  }
  digests {
    sha256: "\274\r\247\254\037TAa\237\252 \202\201\0318\254\367\337\227\344\250\340\217\016\004?\364\223t\024\325\255"
  }
}
library {
  maven_library {
    groupId: "com.squareup.okio"
    artifactId: "okio"
    version: "1.14.0"
  }
  digests {
    sha256: "F3\3031\365\006B\353\347\225\334\b\235jY(\257\3640q\311\321~x@\240\t\356\242\376\225\243"
  }
}
library {
  maven_library {
    groupId: "com.squareup.picasso"
    artifactId: "picasso"
    version: "2.71828"
  }
  digests {
    sha256: "\351\346\263,dW>\331\340\233\205\310\3372\005\260)\337\fo?\017x:\230K\263\373\374\006\350;"
  }
}
library {
  maven_library {
    groupId: "com.squareup.okhttp3"
    artifactId: "okhttp"
    version: "3.10.0"
  }
  digests {
    sha256: "J\312>U*\373\034\353G\370\225f\205\366)Z\'nI\372\003\371\006o\213k\350;\354V\373f"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "exifinterface"
    version: "27.1.0"
  }
  digests {
    sha256: "\224\033_\034\205\272\260\371\231\346:\0009\223\254\267\332\326\300\032\321\221\004\255S\3465)\356-\202\340"
  }
}
library {
  maven_library {
    groupId: "com.squareup.retrofit"
    artifactId: "retrofit"
    version: "1.9.0"
  }
  digests {
    sha256: "\a\r\226\021\220\017\033\263Z\320!fu\335\n\273\246\300\243\034\2316\322\255\210\303;g,\363\342A"
  }
}
library {
  maven_library {
    groupId: "com.google.code.gson"
    artifactId: "gson"
    version: "2.3.1"
  }
  digests {
    sha256: "\023\364J/n\255\005\215\250\n\221\356e\f\a8q\224$h\346\204\251\277j\r\003\031\023\211$\316"
  }
}
library {
  maven_library {
    groupId: "me.leolin"
    artifactId: "ShortcutBadger"
    version: "1.1.21"
  }
  digests {
    sha256: "t\360Pp\227O\256\373K\377\200\357\'\330GX\217\024]\037W\034J\036\272\302\023\216R\340\210*"
  }
}
library {
  maven_library {
    groupId: "com.ablanco.zoomy"
    artifactId: "zoomy"
    version: "1.0.0"
  }
  digests {
    sha256: "\267\005U\002\276\354y?&\323\261/\345w0\370\242\030\215\304\a\337\0258\352\216l\241?\241\367\177"
  }
}
library {
  maven_library {
    groupId: "org.jsoup"
    artifactId: "jsoup"
    version: "1.10.2"
  }
  digests {
    sha256: "n\276j\275wu\301\nI@z\342-\264\\\204\f\322\315\257qXf\245\260\265\257p\224\034?J"
  }
}
library {
  maven_library {
    groupId: "com.google.android.play"
    artifactId: "core"
    version: "1.6.3"
  }
  digests {
    sha256: "\242\357\n<\374\224 X\v\332-\n\203\317m|bR\310\004\017P\275\n\207\364\246H\370\377\306D"
  }
}
library_dependencies {
  library_dep_index: 1
}
library_dependencies {
  library_index: 1
  library_dep_index: 2
  library_dep_index: 3
  library_dep_index: 4
}
library_dependencies {
  library_index: 2
  library_dep_index: 3
  library_dep_index: 5
  library_dep_index: 4
  library_dep_index: 6
}
library_dependencies {
  library_index: 3
  library_dep_index: 7
}
library_dependencies {
  library_index: 7
  library_dep_index: 8
  library_dep_index: 9
  library_dep_index: 10
  library_dep_index: 11
  library_dep_index: 12
}
library_dependencies {
  library_index: 8
  library_dep_index: 13
  library_dep_index: 14
}
library_dependencies {
  library_index: 14
  library_dep_index: 15
  library_dep_index: 16
}
library_dependencies {
  library_index: 9
  library_dep_index: 13
  library_dep_index: 8
}
library_dependencies {
  library_index: 10
  library_dep_index: 13
  library_dep_index: 8
}
library_dependencies {
  library_index: 11
  library_dep_index: 13
  library_dep_index: 8
}
library_dependencies {
  library_index: 12
  library_dep_index: 8
  library_dep_index: 11
  library_dep_index: 10
}
library_dependencies {
  library_index: 5
  library_dep_index: 3
  library_dep_index: 4
}
library_dependencies {
  library_index: 4
  library_dep_index: 3
  library_dep_index: 6
}
library_dependencies {
  library_index: 6
  library_dep_index: 3
}
library_dependencies {
  library_index: 17
  library_dep_index: 18
}
library_dependencies {
  library_index: 19
  library_dep_index: 7
}
library_dependencies {
  library_index: 20
  library_dep_index: 13
  library_dep_index: 7
}
library_dependencies {
  library_index: 22
  library_dep_index: 19
  library_dep_index: 7
}
library_dependencies {
  library_index: 23
  library_dep_index: 5
  library_dep_index: 3
  library_dep_index: 4
}
library_dependencies {
  library_index: 24
  library_dep_index: 25
  library_dep_index: 26
  library_dep_index: 27
  library_dep_index: 3
  library_dep_index: 28
  library_dep_index: 29
}
library_dependencies {
  library_index: 25
  library_dep_index: 3
}
library_dependencies {
  library_index: 26
  library_dep_index: 3
  library_dep_index: 6
}
library_dependencies {
  library_index: 27
  library_dep_index: 26
  library_dep_index: 3
  library_dep_index: 6
}
library_dependencies {
  library_index: 28
  library_dep_index: 3
}
library_dependencies {
  library_index: 29
  library_dep_index: 26
  library_dep_index: 3
  library_dep_index: 6
}
library_dependencies {
  library_index: 30
  library_dep_index: 26
  library_dep_index: 3
  library_dep_index: 6
}
library_dependencies {
  library_index: 31
  library_dep_index: 32
}
library_dependencies {
  library_index: 33
  library_dep_index: 34
  library_dep_index: 13
  library_dep_index: 35
}
library_dependencies {
  library_index: 34
  library_dep_index: 32
}
library_dependencies {
  library_index: 35
  library_dep_index: 13
}
library_dependencies {
  library_index: 36
  library_dep_index: 37
}
module_dependencies {
  module_name: "base"
  dependency_index: 0
  dependency_index: 33
  dependency_index: 36
  dependency_index: 38
  dependency_index: 7
  dependency_index: 39
  dependency_index: 40
  dependency_index: 41
  dependency_index: 17
  dependency_index: 19
  dependency_index: 20
  dependency_index: 21
  dependency_index: 22
  dependency_index: 23
  dependency_index: 24
  dependency_index: 30
  dependency_index: 31
}
