package id.go.kotasurabaya.eperformance.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;

import id.go.kotasurabaya.eperformance.GPSTracker;
import id.go.kotasurabaya.eperformance.R;
import id.go.kotasurabaya.eperformance.adapters.HomeListAdapter;
import id.go.kotasurabaya.eperformance.animation.HidingScrollListener;
import id.go.kotasurabaya.eperformance.data.St;

//public class HomeFragment extends Fragment implements OnClickListener {
public class HomeFragment extends Fragment implements OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    public RecyclerView postsList;
    public View mView;
    public FloatingActionButton mFabButton;
    public Toolbar toolbar;
    public Intent mIntent;
    public LinearLayoutManager layoutManager;
    int currentPage = 1;
    private HomeListAdapter mHomeListAdapter;
    private Context mContext;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.mContext = getActivity().getApplicationContext();
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        initializeView();
        St.getInstance().getMain().setLastFrm("home");
        this.getActivity().setTitle("Dashboard");
        //((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_home);
        //((AppCompatActivity) getActivity()).getSupportActionBar().setShowHideAnimationEnabled(true);
        getPosts();

        GPSTracker gps = new GPSTracker(getActivity());
        if(!gps.canGetLocation()) {
            gps.showSettingsAlert();
        }

        return mView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { //fragmnt
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
    }

    /*@Override
    public void onResume() {
        super.onResume();
        //initializeView();
        //getPosts();
    }*/

    private void initializeView() {

        //setting up our OnScrollListener
        postsList.addOnScrollListener(new HidingScrollListener(layoutManager) {
            @Override
            public void onHide() {
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mFabButton.getLayoutParams();
                int fabBottomMargin = lp.bottomMargin;
                mFabButton.animate().translationY(mFabButton.getHeight() + fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
            }

            @Override
            public void onShow() {
                mFabButton.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void onLoadMore(int currentPage) {
                setCurrentPage(currentPage);
                getPosts();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onResume();
        //M.hideLoadingDialog();
    }

    public void getPosts() {

    }


    @Override
    public void onClick(View v) {

    }
    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }


    @Override
    public void onRefresh() {
        getPosts();
    }
}
