package id.go.kotasurabaya.eperformance.app;

public class AppConst {
    public static final String TAG = "eperformance";
    //public static final String MAIN = "https://eperformance.surabaya.go.id/wsdroid-testbed/service_dev.php";
    //public static final String MAIN = "https://eperformance.surabaya.go.id/wsdroid-testbed/service_dev.php/android.shtml";
    //public static final String MAIN = "https://eperformance.surabaya.go.id/wsdroid-testbed/service_dev.php/android";
    //public static final String MAIN = "https://eperformance.surabaya.go.id/wsdroid-testbed/android";
    //https://eperformance.surabaya.go.id/btb-services/android.shtml
    //public static final String MAIN = "https://eperformance.surabaya.go.id/btb-services/android.shtml";
    //public static final String MAIN = "https://eperformance.surabaya.go.id/wsdroid-testbed/android";//testbed
    public static final String MAIN = "https://eperformance.surabaya.go.id/btb-services/android";//live
    public static final String IMAGE_URL = MAIN + "/image/large/";
    public static final String IMAGE_PROFILE_URL = MAIN + "/image/small/";
    public static final String IMAGE_COVER_URL = MAIN + "/image/cover/";
    public static final String IMAGE_FILE_URL = MAIN + "/file/image/";
    public static final String AUDIO_FILE_URL = MAIN + "/file/audio/";
    public static final String GOOGLE_PROJ_ID = "217561172797";//confComm
    public static final String APK_KEY = "AIzaSyBMQCkVOrpPIWCKNzE9P2bJ22wKG9y27dA";
    public static final int SELECT_PICTURE = 1;
    public static final int SELECT_COVER = 2;
    public static final boolean SHOW_ADS = false;
    public static final int REQUEST_INVITE = 3;
}
