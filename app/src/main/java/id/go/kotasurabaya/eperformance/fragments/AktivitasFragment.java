package id.go.kotasurabaya.eperformance.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import id.go.kotasurabaya.eperformance.GPSTracker;
import id.go.kotasurabaya.eperformance.R;
import id.go.kotasurabaya.eperformance.activities.LoginActivity;
import id.go.kotasurabaya.eperformance.adapters.AktivitasAdapter;
import id.go.kotasurabaya.eperformance.adapters.AktivitasMenungguAdapter;
import id.go.kotasurabaya.eperformance.api.APIService;
import id.go.kotasurabaya.eperformance.api.GlobalAPI;
import id.go.kotasurabaya.eperformance.data.AktivitasModel;
import id.go.kotasurabaya.eperformance.data.St;
import id.go.kotasurabaya.eperformance.helpers.M;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AktivitasFragment extends Fragment implements
        OnClickListener {
    public RecyclerView rowsList;
    public LinearLayoutManager layoutManager;
    public List<AktivitasModel> mItems = new ArrayList<AktivitasModel>();
    public AktivitasAdapter mAdapter;
    public AktivitasMenungguAdapter aMadapter;
    public Intent mIntent = null;
    public EditText searchField;
    public ImageButton searchBtn;
    private View mView;
    SimpleDateFormat dateFormatter;
    Spinner searchBulan;
    String[] items = {"Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember"};
    //private SwipeRefreshLayout mSwipeRefreshLayout;
    int currentPage = 1;
    String currentSearch = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_aktivitas, container,
                false);
        this.getActivity().setTitle("Aktivitas");
        St.getInstance().getMain().setLastFrm("team");

        initializeView();

        GPSTracker gps = new GPSTracker(getActivity());
        if (!gps.canGetLocation()) {
            gps.showSettingsAlert();
        }

        //dateFormatter = new SimpleDateFormat("MM", Locale.US);
        dateFormatter = new SimpleDateFormat("M", Locale.US);
        Date date = new Date();
        searchBulan = (Spinner) mView.findViewById(R.id.searchBulan);
        searchBulan.setAdapter(new MyAdapter(getActivity(), R.layout.row_spinner, items));
        //searchBulan.setOnClickListener(this);
        Integer pos = Integer.parseInt(dateFormatter.format(date));
        searchBulan.setSelection(pos - 1);
        St.getInstance().setAktif_bulan(pos.toString());


        if (St.getInstance().getAktif_status().equalsIgnoreCase("0")) {
            this.getActivity().setTitle("Menunggu Pengesahan");
        } else if (St.getInstance().getAktif_status().equalsIgnoreCase("102")) {
            this.getActivity().setTitle("Tanpa File Pendukung");
        } else if (St.getInstance().getAktif_status().equalsIgnoreCase("1")) {
            this.getActivity().setTitle("Telah Disahkan");
        } else if (St.getInstance().getAktif_status().equalsIgnoreCase("100")) {
            this.getActivity().setTitle("Perlu Revisi");
        } else if (St.getInstance().getAktif_status().equalsIgnoreCase("-1")) {
            this.getActivity().setTitle("Ditolak");
        }

        searchBulan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String searchValue = searchField.getText().toString().trim();

                Integer pos = (searchBulan.getSelectedItemPosition() + 1);
                St.getInstance().setAktif_bulan(pos.toString());
                search(searchValue, 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return mView;
    }


    public class MyAdapter extends ArrayAdapter<String> {

        public MyAdapter(Context context, int textViewResourceId, String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View row = inflater.inflate(R.layout.row_spinner, parent, false);
            TextView label = (TextView) row.findViewById(R.id.company);
            label.setText(items[position]);

            //TextView sub=(TextView)row.findViewById(R.id.sub);
            //sub.setText(subs[position]);

            //ImageView icon=(ImageView)row.findViewById(R.id.image);
            //icon.setImageResource(arr_images[position]);

            return row;
        }
    }


    public void initializeView() {
        rowsList = (RecyclerView) mView.findViewById(R.id.friendsList);
        searchField = (EditText) mView.findViewById(R.id.searchField);
        searchBtn = (ImageButton) mView.findViewById(R.id.searchBtn);
        //layout manager
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        if (St.getInstance().getAktif_status().equalsIgnoreCase("0") ||
                St.getInstance().getAktif_status().equalsIgnoreCase("102") || St.getInstance().getAktif_status().equalsIgnoreCase("100")) {
            aMadapter = new AktivitasMenungguAdapter(getActivity(), mItems);
        } else {
            mAdapter = new AktivitasAdapter(getActivity(), mItems);
        }

        rowsList.setLayoutManager(layoutManager);
        if (St.getInstance().getAktif_status().equalsIgnoreCase("0") ||
                St.getInstance().getAktif_status().equalsIgnoreCase("102") || St.getInstance().getAktif_status().equalsIgnoreCase("100")) {
            rowsList.setAdapter(aMadapter);//set the adapter
        } else {
            rowsList.setAdapter(mAdapter);//set the adapter
        }
        searchBtn.setOnClickListener(this);
//        searchField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
//                if (actionId == EditorInfo.IME_ACTION_DONE || (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
//                    String commentValue = searchField.getText().toString().trim();
//                    setCurrentSearch(commentValue);
//                    search(commentValue, 1); //def cari
//                    return true;
//                }
//                return false;
//            }
//        });
        //
//        mSwipeRefreshLayout = (SwipeRefreshLayout) mView.findViewById(R.id.swipeHome);
//        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                M.L("called");
//                // Refresh items
//                setCurrentPage(1);
//                //getPosts();
//                search(getCurrentSearch(), getCurrentPage());
//            }
//        });
//        //
//        rowsList.addOnScrollListener(new HidingScrollListener(layoutManager) {
//            @Override
//            public void onHide() {
//                //FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mFabButton.getLayoutParams();
//                //int fabBottomMargin = lp.bottomMargin;
//                //mFabButton.animate().translationY(mFabButton.getHeight() + fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
//            }
//
//            @Override
//            public void onShow() {
//                //mFabButton.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
//            }
//
//            @Override
//            public void onLoadMore(int currentPage) {
//                setCurrentPage(currentPage);
//                //getPosts();
//                search(getCurrentSearch(), getCurrentPage());
//            }
//        });
    }

    public void search(String value, Integer pg) {

        //https://eperformance.surabaya.go.id/wsdroid-testbed/android.shtml?obj[action]=getAktivitasPegawaiFilterByBulanOrStatus
        String status = St.getInstance().getAktif_status();
        M.showLoadingDialog(getActivity());
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.aktivitas_lst("getAktivitasPegawaiFilterByBulanOrStatus",
                M.getToken(getActivity()),
                St.getInstance().getLogin().getData().getPegawaiId(),
                St.getInstance().getAktif_bulan(),
                pg.toString(), //"1", //page
                St.getInstance().getJml_per_page(),
                status,
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, retrofit.client.Response response) {
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");
                            mItems = new ArrayList<AktivitasModel>();
                            if (success == -1) {
                                M.logOut(getActivity());
                                mIntent = new Intent(getActivity(), LoginActivity.class);
                                startActivity(mIntent);
                                //finish();
                            }
                            if (success == 1) {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                                Date dt = new Date();
                                Integer g_no = (getCurrentPage() - 1) * Integer.parseInt(St.getInstance().getJml_per_page());
                                JSONArray hasils = data.getJSONArray("aktivitas");
                                for (int i = 0; i < hasils.length(); i++) {
                                    AktivitasModel tmp = new AktivitasModel();
                                    tmp.setAktivitasPegawaiId(hasils.getJSONObject(i).getString("aktivitasPegawaiId"));
                                    tmp.setStruktural(St.getInstance().getLogin().getData().isStrukturalSelainKasieKelurahan());
                                    tmp.setPegawaiId(hasils.getJSONObject(i).getString("pegawaiId"));
                                    tmp.setUpdatedAt(hasils.getJSONObject(i).getString("updatedAt"));
                                    try {
                                        dt = sdf.parse(hasils.getJSONObject(i).getString("tanggal"));
                                        tmp.setTanggal(dt);
                                    } catch (Exception e) {
                                    }
                                    tmp.setStatus(hasils.getJSONObject(i).getString("status"));
                                    tmp.setAktivitasId(hasils.getJSONObject(i).getString("aktivitasId"));
                                    //Toast.makeText(getActivity(), hasils.getJSONObject(i).getString("aktivitasId"), Toast.LENGTH_LONG).show();
                                    if (St.getInstance().getLogin().getData().isStaf() == true || St.getInstance().getLogin().getData().isStrukturalSelainKasieKelurahan() == false) {
                                        tmp.setAktivitasNama(hasils.getJSONObject(i).getString("aktivitasNama"));
                                    } else {
                                        tmp.setWaktuPengerjaan(hasils.getJSONObject(i).getString("menit_waktu_pengerjaan"));
                                        tmp.setJenisWaktuPengerjaan(hasils.getJSONObject(i).getInt("jenis_waktu_pengerjaan"));
                                        tmp.setJumlahOutput(hasils.getJSONObject(i).getString("jumlah_output"));
                                        tmp.setSatuanOutput(hasils.getJSONObject(i).getString("satuan_output"));
                                        if (hasils.getJSONObject(i).getString("aktivitasId").equalsIgnoreCase("null")) {
                                            tmp.setAktivitasNama(hasils.getJSONObject(i).getString("aktivitas_string"));
                                        } else {
                                            tmp.setAktivitasNama(hasils.getJSONObject(i).getString("aktivitasNama"));
                                        }
                                    }
                                    tmp.setBeban(hasils.getJSONObject(i).getString("beban"));
                                    tmp.setCatatan(hasils.getJSONObject(i).getString("catatan"));
                                    tmp.setKomentarAtasan(hasils.getJSONObject(i).getString("komentarAtasan"));
                                    tmp.setKegiatanId(hasils.getJSONObject(i).getString("kegiatanId"));
                                    tmp.setKegiatanNama(hasils.getJSONObject(i).getString("kegiatanNama"));
                                    tmp.setJenis(hasils.getJSONObject(i).getString("jenis"));
                                    tmp.setDariPegawai(hasils.getJSONObject(i).getString("dariPegawai"));
                                    tmp.setJenisWaktuPengerjaan(hasils.getJSONObject(i).getInt("jenis_waktu_pengerjaan"));
                                    tmp.setNomor(g_no + i + 1);
                                    tmp.setLokasiAlamat(hasils.getJSONObject(i).getString("lokasi_alamat"));
                                    tmp.setLokasiKoordinat(hasils.getJSONObject(i).getString("lokasi_koordinat"));
                                    tmp.setApproval8HariKebelakangTerkunci(data.getBoolean("isApproval8HariKebelakangTerkunci"));
                                    ArrayList<String> stringArray = new ArrayList<String>();
                                    JSONArray jsonArray = hasils.getJSONObject(i).getJSONArray("files");
                                    for (int j = 0; j < jsonArray.length(); j++) {
                                        try {
                                            stringArray.add(String.valueOf(jsonArray.get(j)));
                                            System.out.println("File = " + String.valueOf(jsonArray.get(j)));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    tmp.setFiles(stringArray);
                                    mItems.add(tmp);
                                }
                            }
                            //
                            if (getCurrentPage() != 1) {
                                List<AktivitasModel> oldItems;
                                if (St.getInstance().getAktif_status().equalsIgnoreCase("0") ||
                                        St.getInstance().getAktif_status().equalsIgnoreCase("102") || St.getInstance().getAktif_status().equalsIgnoreCase("100")) {
                                    oldItems = aMadapter.getItems();
                                } else {
                                    oldItems = mAdapter.getItems();
                                }
                                oldItems.addAll(mItems);
                                if (St.getInstance().getAktif_status().equalsIgnoreCase("0") ||
                                        St.getInstance().getAktif_status().equalsIgnoreCase("102") || St.getInstance().getAktif_status().equalsIgnoreCase("100")) {
                                    aMadapter.setItems(oldItems);
                                } else {
                                    mAdapter.setItems(oldItems);
                                }
                            } else {
                                if (St.getInstance().getAktif_status().equalsIgnoreCase("0") ||
                                        St.getInstance().getAktif_status().equalsIgnoreCase("102") || St.getInstance().getAktif_status().equalsIgnoreCase("100")) {
                                    aMadapter.setItems(mItems);
                                } else {
                                    mAdapter.setItems(mItems);
                                }
                            }
                            //
//                            if (mSwipeRefreshLayout.isRefreshing()) {
//                                mSwipeRefreshLayout.setRefreshing(false);
//                            }
                        } catch (Exception e) {
                            M.T(getActivity(), e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.T(getActivity(), getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
        M.hideLoadingDialog();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.searchBtn) {
            String searchValue = searchField.getText().toString().trim();
            setCurrentSearch(searchValue);
            Integer pos = (searchBulan.getSelectedItemPosition() + 1);
            St.getInstance().setAktif_bulan(pos.toString());
            search(searchValue, 1);
        }
    }

    private void cekDataForUpdate() {
        //M.showLoadingDialog(getActivity());
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.get_data("getAktivitasPegawaiFilterByAktivitasId",
                M.getToken(getActivity()), "cek",
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, Response response) {
                        M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");

                            if (success == 1) {
                                Toast.makeText(getActivity(), String.valueOf(data),
                                        Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception e) {
                            M.T(getActivity(), e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        M.T(getActivity(), getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }


    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String getCurrentSearch() {
        return currentSearch;
    }

    public void setCurrentSearch(String currentSearch) {
        this.currentSearch = currentSearch;
    }

    @Override
    public void onResume(){
        super.onResume();
        String searchValue = searchField.getText().toString().trim();
        setCurrentSearch(searchValue);
        Integer pos = (searchBulan.getSelectedItemPosition() + 1);
        St.getInstance().setAktif_bulan(pos.toString());
        search(searchValue, 1);
    }
}
