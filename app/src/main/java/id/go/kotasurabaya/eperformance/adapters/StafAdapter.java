package id.go.kotasurabaya.eperformance.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import id.go.kotasurabaya.eperformance.R;
import id.go.kotasurabaya.eperformance.activities.BahawanAktivitas;
import id.go.kotasurabaya.eperformance.activities.BawahanOsAktivitas;
import id.go.kotasurabaya.eperformance.data.St;
import id.go.kotasurabaya.eperformance.data.StafModel;

import com.google.android.gms.ads.AdView;

import java.util.List;

public class StafAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 2;
    private static final int TYPE_ITEM = 1;
    private Activity mActivity;
    private List<StafModel> mItems;
    private LayoutInflater mInflater;

    public StafAdapter(Activity mActivity, List<StafModel> mItems) {
        this.mActivity = mActivity;
        mInflater = LayoutInflater.from(mActivity);
        this.mItems = mItems;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = mInflater.inflate(R.layout.staf_header, parent, false);
            HeaderViewHolder holder = new HeaderViewHolder(view);
            return holder;
        } else {
            View view = mInflater.inflate(R.layout.staf_list_item, parent, false);
            FindHolder holder = new FindHolder(view);
            return holder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FindHolder) {

            StafModel item = mItems.get(position - 1);
            FindHolder findHolder = (FindHolder) holder;
            if (item.getNama() != null) {
                findHolder.username.setText(item.getNama());
            } else {
                findHolder.username.setText(item.getNama());
            }
            String sHtml = item.getJabatan() + " (NULL)";
            Integer iTmp = item.getTotal();
            if (iTmp!=null)
                sHtml = item.getJabatan() + " (<font color='#FF9900'>"+iTmp+"</font>)";
            Spanned spanned = Html.fromHtml(sHtml);
            findHolder.status.setText(spanned);
            /*Picasso.with(mActivity.getApplicationContext())
                    .load(item.getAvatar())
                    .transform(new CropSquareTransformation())
                    .error(R.drawable.image_holder)
                    .placeholder(R.drawable.image_holder)
                    .into(findHolder.picture);*/
            findHolder.nomor.setText(String.valueOf(item.getNomor()));
        } else if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
            {
                headerHolder.mAdView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size() + 1;
        } else {
            return 0;
        }
    }

    public void setUsers(List<StafModel> mItems) {
        this.mItems = mItems;
        notifyDataSetChanged();
    }

    public class FindHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView username, status, nomor;
        ImageView picture;

        public FindHolder(View v) {
            super(v);
            picture = (ImageView) v.findViewById(R.id.picture);
            username = (TextView) v.findViewById(R.id.username);
            status = (TextView) v.findViewById(R.id.status);
            nomor = (TextView) v.findViewById(R.id.nomor);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            StafModel item = mItems.get(this.getPosition() - 1);
            if (item.getNip() != null) {
                Intent FindIntent = new Intent(mActivity, BahawanAktivitas.class);
                FindIntent.putExtra("userID", item.getId());
                St.getInstance().setAktif_bawahan_id(String.valueOf(item.getId()));
                St.getInstance().setAktif_bawahan_nama(item.getNama());
                mActivity.startActivity(FindIntent);
            } else {
                Intent FindIntent = new Intent(mActivity, BawahanOsAktivitas.class);
                FindIntent.putExtra("userID", item.getId());
                St.getInstance().setAktif_bawahan_id(String.valueOf(item.getId()));
                St.getInstance().setAktif_bawahan_nama(item.getNama());
                mActivity.startActivity(FindIntent);
            }
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public AdView mAdView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            mAdView = (AdView) itemView.findViewById(R.id.adView);
        }
    }
}
