package id.go.kotasurabaya.eperformance.activities;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import id.go.kotasurabaya.eperformance.R;

public class FullScreenImageViewActivity extends Activity {
    private LinearLayout llSetWallpaper, llDownloadWallpaper;

    private ImageView fullImageView;
    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().requestFeature(Window.FEATURE_PROGRESS);

        setContentView(R.layout.activity_full_screen_image_view);
        //this.setTitle("Tes");
        //((AppCompatActivity) this).getSupportActionBar().setTitle("Tes");
        //((AppCompatActivity) this).getSupportActionBar().setShowHideAnimationEnabled(true);
        /*
        fullImageView = (ImageView) findViewById(R.id.imgFullscreen);
        llSetWallpaper = (LinearLayout) findViewById(R.id.llSetWallpaper);
        llDownloadWallpaper = (LinearLayout) findViewById(R.id.llDownloadWallpaper);
        llDownloadWallpaper.setOnClickListener(this);
        llSetWallpaper.setOnClickListener(this);
        */

        webview = (WebView) findViewById(R.id.webpage);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setBuiltInZoomControls(true);
        //webview.getSettings().setPluginsEnabled(false);
        final Activity activity = this;
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                activity.setProgress(progress * 1000);
            }
        });
        webview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });

        if (getIntent().hasExtra("image")) {
            String usr = getIntent().getExtras().getString("usr");
            String album_id = getIntent().getExtras().getString("album_id");
            loadImage(usr, album_id, getIntent().getExtras().getString("image"));
        } else {
            finish();
        }
    }

    private void loadImage(String usr,String album_id,String image) {
        String str = "http://hub.solidiance.com/users/" + usr + "/photos/" + album_id + "/big/" + image;
        str = image;
        Log.d("###", str);
        //webview.loadUrl(str);
        String data = "<html><body><img class=\"center\" src=\""+str+"\" width=\"100%\"></body></html>";
        webview.loadData(data, "text/html", "utf-8");
        /*
        Picasso.with(this)
                //.load(AppConst.IMAGE_URL + image)
                ///users/Ipung/photos/43/big/5136d16150ea462c4d469c494fde3e99383aea31.jpg
                .load(str)
                .into(fullImageView);
        Bitmap bitmap = ((BitmapDrawable) fullImageView.getDrawable())
                .getBitmap();
        adjustImageAspect(bitmap.getWidth(), bitmap.getHeight());
        */
    }

    private void adjustImageAspect(int bWidth, int bHeight) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        if (bWidth == 0 || bHeight == 0)
            return;

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int sHeight = size.y;

        int new_width = (int) Math.floor((double) bWidth * (double) sHeight
                / (double) bHeight);
        params.width = new_width;
        params.height = sHeight;

        fullImageView.setLayoutParams(params);
    }


}
