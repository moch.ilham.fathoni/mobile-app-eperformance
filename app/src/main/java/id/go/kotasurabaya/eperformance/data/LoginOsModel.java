package id.go.kotasurabaya.eperformance.data;


public class LoginOsModel {
    SvrData data;
    boolean success;
    String message;

    public SvrData getData() {
        return data;
    }

    public void setData(SvrData data) {
        this.data = data;
    }


    public class SvrData {
        String token, nama, nik, jabatan, atasan, opd;
        int pegawai_id;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getNik() {
            return nik;
        }

        public void setNik(String nik) {
            this.nik = nik;
        }

        public String getJabatan() {
            return jabatan;
        }

        public void setJabatan(String jabatan) {
            this.jabatan = jabatan;
        }

        public String getAtasan() {
            return atasan;
        }

        public void setAtasan(String atasan) {
            this.atasan = atasan;
        }

        public String getOpd() {
            return opd;
        }

        public void setOpd(String opd) {
            this.opd = opd;
        }

        public int getPegawai_id() {
            return pegawai_id;
        }

        public void setPegawai_id(int pegawai_id) {
            this.pegawai_id = pegawai_id;
        }
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
