package id.go.kotasurabaya.eperformance.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;

import java.util.List;

import id.go.kotasurabaya.eperformance.R;
import id.go.kotasurabaya.eperformance.data.PegawaiDuplikasiModel;

public class TaggingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 2;
    private static final int TYPE_ITEM = 1;
    private Activity mActivity;
    private List<PegawaiDuplikasiModel> mItems;
    private LayoutInflater mInflater;

    public TaggingAdapter(Activity mActivity, List<PegawaiDuplikasiModel> mItems) {
        this.mActivity = mActivity;
        mInflater = LayoutInflater.from(mActivity);
        this.mItems = mItems;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = mInflater.inflate(R.layout.staf_header, parent, false);
            HeaderViewHolder holder = new HeaderViewHolder(view);
            return holder;
        } else {
            View view = mInflater.inflate(R.layout.rowlayout, parent, false);
            FindHolder holder = new FindHolder(view);
            return holder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FindHolder) {

            final PegawaiDuplikasiModel item = mItems.get(position - 1);
            FindHolder findHolder = (FindHolder) holder;
            if (item.getNama() != null) {
                findHolder.namaTag.setText(item.getNama());
            } else {
                findHolder.namaTag.setText(item.getNama());
            }
            findHolder.jabatanTag.setText(item.getJabatan());
            findHolder.chk.setChecked(item.isChecked());
            if (item.isChecked()) findHolder.chk.setChecked(true);
            //findHolder.chk.setOnCheckedChangeListener(null);
            findHolder.chk.setTag(position);
            findHolder.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    item.setChecked(b);
                }
            });
        } else if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
            {
                headerHolder.mAdView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size() + 1;
        } else {
            return 0;
        }
    }

    public void setItems(List<PegawaiDuplikasiModel> itemModels) {
        this.mItems = itemModels;
        notifyDataSetChanged();
    }

    public class FindHolder extends RecyclerView.ViewHolder {
        TextView namaTag, jabatanTag, nomor;
        CheckBox chk;

        public FindHolder(View v) {
            super(v);
            namaTag = (TextView) v.findViewById(R.id.namaTag);
            jabatanTag = (TextView) v.findViewById(R.id.jabatanTag);
            chk = (CheckBox) v.findViewById(R.id.chkTag);
            this.setIsRecyclable(false);
            //v.setOnClickListener(this);
        }

    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public AdView mAdView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            mAdView = (AdView) itemView.findViewById(R.id.adView);
        }
    }
}
