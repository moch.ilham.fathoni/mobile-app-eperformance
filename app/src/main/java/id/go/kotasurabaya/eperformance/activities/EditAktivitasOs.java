package id.go.kotasurabaya.eperformance.activities;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import id.go.kotasurabaya.eperformance.PermissionManager;
import id.go.kotasurabaya.eperformance.PictureUtilities;
import id.go.kotasurabaya.eperformance.R;
import id.go.kotasurabaya.eperformance.adapters.ImageAdapter;
import id.go.kotasurabaya.eperformance.api.APIService;
import id.go.kotasurabaya.eperformance.api.GlobalAPI;
import id.go.kotasurabaya.eperformance.data.MasterAktivitasOsModel;
import id.go.kotasurabaya.eperformance.data.PenugasanKegiatanOsModel;
import id.go.kotasurabaya.eperformance.data.St;
import id.go.kotasurabaya.eperformance.helpers.CustomAlertAdapter;
import id.go.kotasurabaya.eperformance.helpers.M;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

public class EditAktivitasOs extends Activity implements View.OnClickListener {

    TextView tglAktivitas, lblAlbum, lblKamera, hapusFoto;
    private Button btnUbah;
    EditText tanggal, aktivitas, kegiatan, catatan;
    RadioGroup rg;
    String aktivitas_id, kegiatan_id, note;
    int jenis_waktu_pengerjaan = 0;
    AlertDialog.Builder builderKeg, builderAkt;
    private SimpleDateFormat dateFormatter, ymdFormatter;
    private DatePickerDialog tanggalDlg;
    private SimpleDateFormat bulanFormatter;
    String id_entry = "";
    String tambahan = "";

    List<PenugasanKegiatanOsModel> kegiatans;
    List<MasterAktivitasOsModel> master_aktivitas;
    String jenis = "0", tanggal_ymd;

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private ImageButton btnPilihCamera, btnPilihAlbum;
    String koordinat, alamat;
    String imagePath = "";
    List<String> fixPath = new ArrayList<>();
    ContentValues values;
    Uri imageUri;
    ListView lView;
    ImageAdapter lAdapter;
    PermissionManager permissions;
    ImageView gambar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        this.setTitle("Edit Aktivitas");
        permissions = new PermissionManager();

        Intent intent = getIntent();

        builderKeg = new AlertDialog.Builder(this);
        builderAkt = new AlertDialog.Builder(this);

        if (!permissions.checkPermissionCameraStorage(this)) {
            permissions.setPermissionCameraStorage(this);
        } else if (!permissions.checkPermissionCamera(this)) {
            permissions.setPermissionCamera(this);
        } else if (!permissions.checkPermissionStorage(this)) {
            permissions.setPermissionStorage(this);
        }

        btnPilihCamera = (ImageButton) findViewById(R.id.btnPilihCameraEditStaff);
        lblKamera = (TextView) findViewById(R.id.lbl_kamera);
        btnPilihAlbum = (ImageButton) findViewById(R.id.btnPilihAlbumEditStaff);
        lblAlbum = (TextView) findViewById(R.id.lbl_album);
        hapusFoto = (TextView) findViewById(R.id.lbl_image_pendukung);
        gambar = (ImageView) findViewById(R.id.imagePendukung);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        ymdFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        bulanFormatter = new SimpleDateFormat("MM", Locale.US);

        tglAktivitas = (TextView) findViewById(R.id.aktivitasTanggalEditStaff);

        tglAktivitas.setText(dateFormatter.format((Date) intent.getSerializableExtra("aktivitas_tanggal")));
        tanggal_ymd = ymdFormatter.format((Date) intent.getSerializableExtra("aktivitas_tanggal"));
        System.out.println((Date) intent.getSerializableExtra("aktivitas_tanggal"));
        aktivitas = (EditText) findViewById(R.id.aktivitasEditStaff);
        aktivitas.setText(intent.getStringExtra("aktivitas_nama"));
        System.out.println(intent.getStringExtra("aktivitas_nama"));
        aktivitas_id = intent.getStringExtra("aktivitas_id");
        System.out.println(intent.getStringExtra("aktivitas_id"));
        btnUbah = (Button) findViewById(R.id.btnUbahStaff);
        kegiatan = (EditText) findViewById(R.id.kegiatanEditStaff);
        kegiatan.setText(intent.getStringExtra("aktivitas_kegiatan_nama"));
        System.out.println(intent.getStringExtra("aktivitas_kegiatan_nama"));
        kegiatan_id = intent.getStringExtra("aktivitas_kegiatan_id");
        System.out.println(intent.getStringExtra("aktivitas_kegiatan_id"));
        catatan = (EditText) findViewById(R.id.catatanEditStaff);
        catatan.setText(intent.getStringExtra("aktivitas_catatan"));
        note = intent.getStringExtra("aktivitas_catatan");
        System.out.println(intent.getStringExtra("aktivitas_catatan"));
        id_entry = intent.getStringExtra("id_entry");
        System.out.println(id_entry);
        koordinat = intent.getStringExtra("aktivitas_koordinat");
        System.out.println(intent.getStringExtra("aktivitas_koordinat"));
        alamat = intent.getStringExtra("aktivitas_alamat");
        System.out.println(intent.getStringExtra("aktivitas_alamat"));
        fixPath = intent.getStringArrayListExtra("aktivitas_files");
        System.out.println(intent.getStringArrayListExtra("aktivitas_files"));
        updateImage();
        rg = (RadioGroup) findViewById(R.id.radioGroupAktivitasEditStaff);
        if (intent.getStringExtra("aktivitas_jenis_waktu_pengerjaan").equalsIgnoreCase("Aktivitas pada jam kerja")) {
            rg.check(R.id.rbJamKerjaEditStaff);
            jenis_waktu_pengerjaan = 0;
        } else if (intent.getStringExtra("aktivitas_jenis_waktu_pengerjaan").equalsIgnoreCase("Aktivitas diluar jam kerja")) {
            rg.check(R.id.rbLuarJamKerjaEditStaff);
            jenis_waktu_pengerjaan = 1;
        }
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbJamKerjaEditStaff:
                        // do operations specific to this selection
                        jenis_waktu_pengerjaan = 0;
                        break;
                    case R.id.rbLuarJamKerjaEditStaff:
                        // do operations specific to this selection
                        jenis_waktu_pengerjaan = 1;
                        break;
                }
            }
        });
        Date date = new Date();
        tanggal_ymd = ymdFormatter.format(date);
        Calendar newCalendar = Calendar.getInstance();
        tanggalDlg = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tanggal.setText(dateFormatter.format(newDate.getTime()));
                tanggal_ymd = ymdFormatter.format(newDate.getTime());
                getMaster(ymdFormatter.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        aktivitas.setOnClickListener(this);
        kegiatan.setOnClickListener(this);
        btnUbah.setOnClickListener(this);
        btnPilihCamera.setOnClickListener(this);
        btnPilihAlbum.setOnClickListener(this);
        hapusFoto.setOnClickListener(this);
        hapusFoto.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    hapusFoto.setTextColor(getResources().getColor(R.color.link_text_hover));
                }
                if(event.getAction() == MotionEvent.ACTION_UP){
                    hapusFoto.setTextColor(getResources().getColor(R.color.link_text));
                }
                return false;
            }
        });

        getMaster(ymdFormatter.format(date));


    }

    private void getMaster(String tanggal) {
        M.showLoadingDialog(this);
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(this));
        api.master_os("validasiEntriAktivitasByTanggalAktivitas",
                M.getToken(this), tanggal,
                St.getInstance().getLoginOs().getData().getPegawai_id(),
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, Response response) {
                        M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            boolean success = obj.getBoolean("success");
                            String message = obj.getString("message");
                            kegiatans = new ArrayList<PenugasanKegiatanOsModel>();
                            master_aktivitas = new ArrayList<MasterAktivitasOsModel>();
                            if (success) {
                                JSONObject data = obj.getJSONObject("data");
                                JSONObject akt = data.getJSONObject("aktivitasMaster");
                                JSONObject keg = data.getJSONObject("penugasanKegiatan");
                                for (int i = 0; i < akt.length(); i++) {
                                    MasterAktivitasOsModel tmp = new MasterAktivitasOsModel();
                                    String id = akt.names().opt(i).toString();
                                    tmp.setId(id);
                                    tmp.setNama(akt.getString(id));
                                    master_aktivitas.add(tmp);
                                }
                                for (int i = 0; i < keg.length(); i++) {
                                    PenugasanKegiatanOsModel tmp = new PenugasanKegiatanOsModel();
                                    String id = keg.names().opt(i).toString();
                                    tmp.setId(id);
                                    tmp.setNama(keg.getString(id));
                                    kegiatans.add(tmp);
                                }
                            }
                        } catch (Exception e) {
                            M.T(EditAktivitasOs.this, e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        M.T(EditAktivitasOs.this, getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }

    private void doSimpan() {
        M.showLoadingDialog(this);
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(this));
        if (kegiatan_id.isEmpty() || kegiatan_id.equalsIgnoreCase("") || kegiatan_id == null) {
            kegiatan_id = "null";
        }
        api.aktivitas_os_simpan(data("simpanAktivitasPegawai", M.getToken(this),
                String.valueOf(St.getInstance().getLoginOs().getData().getPegawai_id()), id_entry,
                tanggal_ymd, aktivitas_id, String.valueOf(jenis_waktu_pengerjaan),
                kegiatan_id, catatan.getText().toString().trim() + tambahan,
                koordinat, alamat, imagePath),
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, Response response) {
                        M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            boolean success = obj.getBoolean("success");
                            String message = obj.getString("message");
                            M.T(EditAktivitasOs.this, message);
                            if (success) {
                                finish();
                                Intent main = new Intent(EditAktivitasOs.this, MainActivity2.class);
                                main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                main.putExtra("edit", "success");
                                startActivity(main);
                            } else {
                                tanggal.setError("tanggal invalid");
                            }
                        } catch (Exception e) {
                            M.T(EditAktivitasOs.this, e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        M.T(EditAktivitasOs.this, getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }


    private ArrayList<String> array_sort;
    private String ars_aktivitas[];

    @Override
    public void onClick(View v) {
        AlertDialog alertCat;
        List<String> lst = new ArrayList<>();
        final String[] ars;
        if (v.getId() == R.id.btnUbahStaff) {
            if (aktivitas_id == "" || aktivitas_id == null || aktivitas_id.equalsIgnoreCase("") ||
                    aktivitas_id.isEmpty() || aktivitas_id.equalsIgnoreCase("0")) {
                aktivitas.setError("Pilih ulang aktivitas terlebih dahulu");
                Toast.makeText(this, "Pilih ulang aktivitas terlebih dahulu",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (kegiatan_id == "" || kegiatan_id == null) {
                aktivitas.setError("Pilih ulang kegiatan terlebih dahulu");
                Toast.makeText(this, "Pilih ulang kegiatan terlebih dahulu",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (catatan.getText().toString().equalsIgnoreCase("") || catatan.length() < 15) {
                catatan.setError("Isi catatan minimal 15 karakter");
                Toast.makeText(this, "Isi catatan minimal 15 karakter",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (fixPath.isEmpty()) {
                Toast.makeText(EditAktivitasOs.this, "Unggah data pendukung terlebih dahulu !",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (catatan.getText().toString().equalsIgnoreCase(note)) {
                tambahan = " ";
//                catatan.setError("Ubah catatan terlebih dahulu");
//                Toast.makeText(this, "Ubah catatan terlebih dahulu",
//                        Toast.LENGTH_LONG).show();
//                return;
            }

            doSimpan();
        } else if (v.getId() == R.id.aktivitasEditStaff) {
            if (master_aktivitas == null) {
                aktivitas.setError("Gagal ambil info aktivitas");
                return;
            }
            for (MasterAktivitasOsModel s : master_aktivitas) {
                lst.add(s.getNama());
            }
            ars = lst.toArray(new String[lst.size()]);
            ars_aktivitas = ars;
            builderAkt.setTitle("Pilih aktivitas");
            do_aktivitas();

        } else if (v.getId() == R.id.kegiatanEditStaff) {
            if (kegiatans == null) {
                kegiatan.setError("Gagal ambil info kegiatan");
                return;
            }
            for (PenugasanKegiatanOsModel s : kegiatans) {
                lst.add(s.getNama());
            }
            ars = lst.toArray(new String[lst.size()]);
            builderKeg.setTitle("Pilih kegiatan");
            builderKeg.setItems(ars, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String str = ars[item];
                    if (str.equalsIgnoreCase("none"))
                        str = "";
                    kegiatan.setText(str);
                    for (PenugasanKegiatanOsModel s : kegiatans) {
                        if (s.getNama().equalsIgnoreCase(str)) {
                            kegiatan_id = s.getId();
                            break;
                        }
                    }
                }
            });
            alertCat = builderKeg.create();
            alertCat.show();
        } else if (v.getId() == R.id.btnPilihCameraEditStaff) {
            if (!permissions.checkPermissionCameraStorage(this)) {
                permissions.setPermissionCameraStorage(this);
            } else if (!permissions.checkPermissionCamera(this)) {
                permissions.setPermissionCamera(this);
            } else if (!permissions.checkPermissionStorage(this)) {
                permissions.setPermissionStorage(this);
            } else {
                cameraIntent();
            }
        } else if (v.getId() == R.id.btnPilihAlbumEditStaff) {
            if (!permissions.checkPermissionCameraStorage(this)) {
                permissions.setPermissionCameraStorage(this);
            } else if (!permissions.checkPermissionCamera(this)) {
                permissions.setPermissionCamera(this);
            } else if (!permissions.checkPermissionStorage(this)) {
                permissions.setPermissionStorage(this);
            } else {
                galleryIntent();
            }
        } else if (v.getId() == R.id.lbl_image_pendukung) {
            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(EditAktivitasOs.this);
            alertDialogBuilder.setMessage("Apakah Anda Ingin Menghapus Gambar ini ?");
            alertDialogBuilder.setPositiveButton("Ya",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            if (fixPath.size() > 0) {
                                String[] data = fixPath.get(0).split("\\|");
                                if (data.length < 3) {
                                    fixPath.remove(0);
                                } else {
                                    hapusGambar(data[0], 0);
                                }
                                imagePath = "";
                                updateImage();
                            }
                        }
                    });

            alertDialogBuilder.setNegativeButton("Tidak",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                        }
                    });

            //Showing the alert dialog
            android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/jpeg");
        //intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "Eperformance Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        imageUri = EditAktivitasOs.this.getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, REQUEST_CAMERA);
        values.clear();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }

    }

    private void onCaptureImageResult(Intent data) {
        String currentDateandTime = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
        ContentResolver cr = EditAktivitasOs.this.getContentResolver();
        InputStream is = null;
        try {
            is = cr.openInputStream(imageUri);
            cr.getType(imageUri);
            int size = is.available();
            Log.d("Size Image Capture: ", "" + PictureUtilities.getFileSize(size));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (Math.round(PictureUtilities.getFileSize(EditAktivitasOs.this, imageUri)) < 2) {
            imagePath = PictureUtilities.getRealPathFromURI(EditAktivitasOs.this, String.valueOf(imageUri));
        } else {
            imagePath = PictureUtilities.compressImage(EditAktivitasOs.this, String.valueOf(imageUri), currentDateandTime);
        }
        //realPath.add(PictureUtilities.compressImage(getActivity(), String.valueOf(imageUri), currentDateandTime));
        fixPath.clear();
        fixPath.add(imagePath);
        updateImage();
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        String currentDateandTime = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
        Bitmap bitmap = null;
        if (data.getData() != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(EditAktivitasOs.this.getContentResolver(), data.getData());
                imageUri = data.getData();
                if (Math.round(PictureUtilities.getFileSize(EditAktivitasOs.this, imageUri)) < 2) {
                    imageUri = PictureUtilities.getImageUri(EditAktivitasOs.this, bitmap);
                    imagePath = PictureUtilities.getRealPathFromURI(EditAktivitasOs.this, imageUri.toString());
                } else {
                    imageUri = PictureUtilities.getImageUri(EditAktivitasOs.this, bitmap);
                    imagePath = PictureUtilities.compressImage(EditAktivitasOs.this, String.valueOf(imageUri), currentDateandTime);
                }
                fixPath.clear();
                fixPath.add(imagePath);
                updateImage();
                //realPath.add(PictureUtilities.compressImage(getActivity(), String.valueOf(imageUri), currentDateandTime));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            if (data.getClipData() != null) {
                ClipData clipData = data.getClipData();
                for (int i = 0; i < clipData.getItemCount(); i++) {
                    ClipData.Item item = clipData.getItemAt(i);

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(EditAktivitasOs.this.getContentResolver(), item.getUri());
                        //Log.d("Image get: ", "" + PictureUtilities.compressImage(getActivity(), String.valueOf(PictureUtilities.getImageUri(getActivity(), bitmap)), currentDateandTime));
                        //realPath.add(PictureUtilities.compressImage(getActivity(), String.valueOf(PictureUtilities.getImageUri(getActivity(), bitmap)), currentDateandTime + "_" + i));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

        }
//        updateImageList();
    }

//    private void updateImageList() {
//        final String[] arp;
//        arp = fixPath.toArray(new String[fixPath.size()]);
//        lView = (ListView) findViewById(R.id.listImageEditStaff);
//        lAdapter = new ImageAdapter(this, arp);
//        lView.setAdapter(lAdapter);
//
//        lView.setOnTouchListener(new View.OnTouchListener() {
//            // Setting on Touch Listener for handling the touch inside ScrollView
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                // Disallow the touch request for parent scroll on touch of child view
//                v.getParent().requestDisallowInterceptTouchEvent(true);
//                return false;
//            }
//        });
//
//        lView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id) {
//                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(EditAktivitasOs.this);
//                alertDialogBuilder.setMessage("Apakah Anda Ingin Menghapus Gambar ini ?");
//                alertDialogBuilder.setPositiveButton("Ya",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface arg0, int arg1) {
//                                String[] data = fixPath.get(position).split("\\|");
//                                if (data.length < 3) {
//                                    fixPath.remove(position);
//                                } else {
//                                    hapusGambar(data[0], position);
//                                }
//                                imagePath = "";
//                                updateImageList();
//                            }
//                        });
//
//                alertDialogBuilder.setNegativeButton("Tidak",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface arg0, int arg1) {
//
//                            }
//                        });
//
//                //Showing the alert dialog
//                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
//                alertDialog.show();
//            }
//        });
//        if (fixPath.isEmpty()) {
//            btnPilihAlbum.setVisibility(View.VISIBLE);
//            lblAlbum.setVisibility(View.VISIBLE);
//            btnPilihCamera.setVisibility(View.VISIBLE);
//            lblKamera.setVisibility(View.VISIBLE);
//        } else {
//            btnPilihCamera.setVisibility(View.GONE);
//            lblKamera.setVisibility(View.GONE);
//            btnPilihAlbum.setVisibility(View.GONE);
//            lblAlbum.setVisibility(View.GONE);
//        }
//    }

    private void updateImage() {
        final String[] arp;
        arp = fixPath.toArray(new String[fixPath.size()]);
        if (fixPath.size() > 0) {
            if (arp[0].contains("https")) {
                String[] data = arp[0].split("\\|");
                if (data[2].toString().contains("image/")) {
                    Picasso.get().load(data[3]).into(gambar);
                } else {
                    gambar.setImageResource(0);
                }
            } else {
                gambar.setImageURI(Uri.fromFile(new File(arp[0])));
            }
        } else {
            gambar.setImageResource(0);
            gambar.setImageDrawable(null);
        }
        System.out.println("Array gambar = " + fixPath.size());

        if (fixPath.isEmpty()) {
            btnPilihAlbum.setVisibility(View.VISIBLE);
            lblAlbum.setVisibility(View.VISIBLE);
            btnPilihCamera.setVisibility(View.VISIBLE);
            lblKamera.setVisibility(View.VISIBLE);
            hapusFoto.setVisibility(View.GONE);
        } else {
            btnPilihCamera.setVisibility(View.GONE);
            lblKamera.setVisibility(View.GONE);
            btnPilihAlbum.setVisibility(View.GONE);
            lblAlbum.setVisibility(View.GONE);
            hapusFoto.setVisibility(View.VISIBLE);
        }
    }

    private MultipartTypedOutput data(String action, String token, String idPegawai, String id, String tanggal,
                                      String idAktivitas, String jenisWaktuPengerjaan, String idKegiatan,
                                      String catatan, String koordinat, String lokasi, String path) {
        MultipartTypedOutput multipartTypedOutput = new MultipartTypedOutput();
        multipartTypedOutput.addPart("obj[action]", new TypedString(action));
        multipartTypedOutput.addPart("obj[token]", new TypedString(token));
        multipartTypedOutput.addPart("obj[pegawai_id]", new TypedString(idPegawai));
        multipartTypedOutput.addPart("obj[id]", new TypedString(id));
        multipartTypedOutput.addPart("obj[tanggal]", new TypedString(tanggal));
        multipartTypedOutput.addPart("obj[aktivitas_id]", new TypedString(idAktivitas));
        multipartTypedOutput.addPart("obj[jenis_waktu_pengerjaan]", new TypedString(jenisWaktuPengerjaan));
        multipartTypedOutput.addPart("obj[kegiatan_id]", new TypedString(idKegiatan));
        multipartTypedOutput.addPart("obj[catatan]", new TypedString(catatan));
        multipartTypedOutput.addPart("obj[lokasi_koordinat]", new TypedString(koordinat));
        multipartTypedOutput.addPart("obj[lokasi_alamat]", new TypedString(lokasi));
        if (path == "" || path == null) {

        } else {
            multipartTypedOutput.addPart("files[]", new TypedFile("image/jpeg", new File(path)));
        }
        //        if(path.isEmpty()){
//
//        } else {
//            for (int i = 0; i < path.size(); i++) {
//                multipartTypedOutput.addPart("files[]", new TypedFile("image/jpg", new File(path.get(i))));
//            }
//        }

        return multipartTypedOutput;
    }

    int textlength = 0;
    private AlertDialog myalertDialog = null;

    void do_aktivitas() {
        final EditText editText = new EditText(this);
        final ListView listview = new ListView(this);
        editText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_search, 0, 0, 0);
        array_sort = new ArrayList<String>(Arrays.asList(ars_aktivitas));
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.addView(editText);
        layout.addView(listview);
        builderAkt.setView(layout);
        CustomAlertAdapter arrayAdapter = new CustomAlertAdapter(this, array_sort);
        listview.setAdapter(arrayAdapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                //alertCat.dismiss();
                myalertDialog.dismiss();
                //String str = ars_aktivitas[position];
                String str = array_sort.get(position).toString();
                if (str.equalsIgnoreCase("none"))
                    str = "";
                aktivitas.setText(str);
                for (MasterAktivitasOsModel s : master_aktivitas) {
                    if (s.getNama().equalsIgnoreCase(str)) {
                        aktivitas_id = s.getId();
                        break;
                    }
                }
            }
        });
        editText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s,
                                          int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                textlength = editText.getText().length();
                array_sort.clear();
                for (int i = 0; i < ars_aktivitas.length; i++) {
                    if (textlength <= ars_aktivitas[i].length()) {

                        if (ars_aktivitas[i].toLowerCase().contains(editText.getText().toString().toLowerCase().trim())) {
                            array_sort.add(ars_aktivitas[i]);
                        }
                    }
                }
                listview.setAdapter(new CustomAlertAdapter(EditAktivitasOs.this, array_sort));
                builderAkt.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
        });
        myalertDialog = builderAkt.show();
    }

    private void hapusGambar(String idFile, final int posisi) {
        M.showLoadingDialog(EditAktivitasOs.this);
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(EditAktivitasOs.this));
        api.hapus_file_os("hapusFotoPendukung",
                M.getToken(EditAktivitasOs.this),
                idFile,
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, Response response) {
                        M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            boolean success = obj.getBoolean("success");
                            String message = obj.getString("message");
                            if (success) {
                                fixPath.remove(posisi);
                                updateImage();
                            }
                            M.T(EditAktivitasOs.this, message);
                        } catch (Exception e) {
                            M.T(EditAktivitasOs.this, e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        M.T(EditAktivitasOs.this, getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
