package id.go.kotasurabaya.eperformance.data;


public class MasterAktivitasModel {
    String aktivitasId, nama, beban;

    public String getAktivitasId() {
        return aktivitasId;
    }

    public void setAktivitasId(String aktivitasId) {
        this.aktivitasId = aktivitasId;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getBeban() {
        return beban;
    }

    public void setBeban(String beban) {
        this.beban = beban;
    }
}
