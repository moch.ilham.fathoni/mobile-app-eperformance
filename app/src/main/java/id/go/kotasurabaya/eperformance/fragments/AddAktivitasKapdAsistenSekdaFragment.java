package id.go.kotasurabaya.eperformance.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import id.go.kotasurabaya.eperformance.FileUtils;
import id.go.kotasurabaya.eperformance.GPSTracker;
import id.go.kotasurabaya.eperformance.ImagePicker;
import id.go.kotasurabaya.eperformance.PermissionManager;
import id.go.kotasurabaya.eperformance.PinchZoomImageView;
import id.go.kotasurabaya.eperformance.R;
import id.go.kotasurabaya.eperformance.adapters.TaggingAdapter;
import id.go.kotasurabaya.eperformance.api.APIService;
import id.go.kotasurabaya.eperformance.api.GlobalAPI;
import id.go.kotasurabaya.eperformance.data.MasterAktivitasModel;
import id.go.kotasurabaya.eperformance.data.PegawaiDuplikasiModel;
import id.go.kotasurabaya.eperformance.data.PenugasanKegiatanModel;
import id.go.kotasurabaya.eperformance.data.St;
import id.go.kotasurabaya.eperformance.helpers.CustomAlertAdapter;
import id.go.kotasurabaya.eperformance.helpers.M;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

public class AddAktivitasKapdAsistenSekdaFragment extends Fragment implements OnClickListener {
    public View mView;
    public View dialogView;
    public View rowView;
    private Button btnSave;
    private Button btnSave2;
    TextView lblDuplikasi;
    CheckBox check;
    EditText tanggal, aktivitas, kegiatan, catatan, tag, find;
    //    EditText menit_waktu_pengerjaan, satuan_output, jumlah_output;
    RadioGroup rg;
    String aktivitas_string;
    int jenis_waktu_pengerjaan = 0;
    AlertDialog.Builder builderKeg, builderAkt, builderPeg;
    private SimpleDateFormat dateFormatter, ymdFormatter;
    private DatePickerDialog tanggalDlg;
    private SimpleDateFormat bulanFormatter;

    List<PenugasanKegiatanModel> kegiatans;
    List<PegawaiDuplikasiModel> mItems = new ArrayList<PegawaiDuplikasiModel>();
    List<MasterAktivitasModel> master_aktivitas;

    final List<String> selectedPegawaiNamaTag = new ArrayList<>();
    final List<String> selectedPegawaiIdTag = new ArrayList<>();
    final List<String> kegiatan_id = new ArrayList<>();
    final List<String> kegiatan_nama = new ArrayList<>();
    final List<String> kegiatan_jenis = new ArrayList<>();

    String jenis = "0", tanggal_ymd;
    String setbulan;
    public RecyclerView mList;
    public LinearLayoutManager layoutManager;
    public TaggingAdapter mAdapter;

    View myView;
    TextView showMore;
    boolean isShow;

    private boolean capaian;
    private static final int REQUEST_IMAGE_PICK = 0;
    private ImageButton btnPilihGambar;
    String lokasi_koordinat, lokasi_alamat;
    //final List<String> realPath = new ArrayList<>();
    ContentValues values;
    Uri imageUri;
    //ListView lView;
    //ImageAdapter lAdapter;
    String imagePath = null;
    ImageView lampiran;
    GPSTracker gps;
    //Location lokasi = null;
    TextView lokasilbl, lblPilihGambar, lblPercent, jmlPercent;
    ImageButton btnSinkron;
    //GPSHelper gh;
    PinchZoomImageView pinchZoomImageView;
    PermissionManager permissions;
    LinearLayout layoutPercent;
    ProgressBar percent;
    int persentase = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.activity_addnew_kapdasistensekda, container,
                false);

        this.getActivity().setTitle("Entry Aktivitas");
        permissions = new PermissionManager();

        builderKeg = new AlertDialog.Builder(getActivity());
        builderAkt = new AlertDialog.Builder(getActivity());
        builderPeg = new AlertDialog.Builder(getActivity());

        if (!permissions.checkPermissionLocationCameraStorage(getActivity())) {
            permissions.setPermissionLocationCameraStorage(getActivity());
        } else if (!permissions.checkPermissionLocationCamera(getActivity())) {
            permissions.setPermissionLocationCamera(getActivity());
        } else if (!permissions.checkPermissionLocationStorage(getActivity())) {
            permissions.setPermissionLocationStorage(getActivity());
        } else if (!permissions.checkPermissionCameraStorage(getActivity())) {
            permissions.setPermissionCameraStorage(getActivity());
        } else if (!permissions.checkPermissionLocation(getActivity())) {
            permissions.setPermissionLocation(getActivity());
        } else if (!permissions.checkPermissionCamera(getActivity())) {
            permissions.setPermissionCamera(getActivity());
        } else if (!permissions.checkPermissionStorage(getActivity())) {
            permissions.setPermissionStorage(getActivity());
        }

        initializeView();
        showMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isShow) {
                    android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setMessage("Apakah Anda yakin atribut lain yang akan disembunyikan tidak perlu disimpan ?");
                    alertDialogBuilder.setPositiveButton("Ya",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
//                                    jumlah_output.setText("");
//                                    satuan_output.setText("");
                                    kegiatan.setText("");
                                    catatan.setText("");
                                    tag.setText("");
                                    selectedPegawaiNamaTag.clear();
                                    selectedPegawaiIdTag.clear();
                                    kegiatan_id.clear();
                                    kegiatan_nama.clear();
                                    kegiatan_jenis.clear();
                                    getKegiatan(setbulan);
                                    slideDown(myView);
                                    showMore.setText("Tampilkan lainnya");
                                    isShow = false;
                                }
                            });

                    alertDialogBuilder.setNegativeButton("Tidak",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {

                                }
                            });

                    //Showing the alert dialog
                    android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else {
                    slideUp(myView);
                    showMore.setText("Sembunyikan lainnya");
                    isShow = true;
                }
                //isShow = !isShow;
            }
        });

        gps = new GPSTracker(getActivity());
        sinkronLokasi();

        return mView;
    }

    private void getRealisasi(String bulan) {
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.get_target_realisasi("getTargetRealisasiAktivitasPerbulan",
                M.getToken(getActivity()),
                St.getInstance().getLogin().getData().getPegawaiId(),
                bulan,
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, Response response) {
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");
                            if (success == 1) {
                                Integer target = data.getInt("target");
                                Integer realisasi = data.getInt("realisasi");
                                if (realisasi >= target) {
                                    persentase = 100;
                                } else {
                                    persentase = (int) (realisasi * 100 / target);
                                }
                                System.out.println("Presentase = " + persentase);
                                percent.setProgress(persentase);
                                Drawable progressDrawable = percent.getProgressDrawable();
                                if (persentase > 50) {
                                    progressDrawable.setColorFilter(Color.GREEN, android.graphics.PorterDuff.Mode.MULTIPLY);
                                } else {
                                    progressDrawable.setColorFilter(Color.YELLOW, android.graphics.PorterDuff.Mode.MULTIPLY);
                                }
                                percent.setProgressDrawable(progressDrawable);
                                jmlPercent.setText(persentase + "%");
                            }
                        } catch (Exception e) {
                            M.T(getActivity(), e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.T(getActivity(), getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }

    private void getKegiatan(String bulan) {
        M.showLoadingDialog(getActivity());
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.get_kegiatan("getDaftarPenugasanKegiatanPegawaiPerbulan",
                M.getToken(getActivity()),
                St.getInstance().getLogin().getData().getPegawaiId(),
                bulan,
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, retrofit.client.Response response) {
                        M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");
                            kegiatans = new ArrayList<PenugasanKegiatanModel>();
                            if (success == 1) {
                                JSONArray hasils = data.getJSONArray("penugasanKegiatan");
                                for (int i = 0; i < hasils.length(); i++) {
                                    PenugasanKegiatanModel tmp = new PenugasanKegiatanModel();
                                    tmp.setId(hasils.getJSONObject(i).getString("id"));
                                    tmp.setKode(hasils.getJSONObject(i).getString("kode"));
                                    tmp.setNama(hasils.getJSONObject(i).getString("nama"));
                                    tmp.setJenis(hasils.getJSONObject(i).getString("jenis"));
                                    tmp.setStatus(false);
                                    kegiatans.add(tmp);
                                }
                            }
                            for (PenugasanKegiatanModel s : kegiatans) {
                                for (int i = 0; i < kegiatan_id.size(); i++) {
                                    if (s.getId().equalsIgnoreCase(kegiatan_id.get(i))) {
                                        s.setStatus(true);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            M.T(getActivity(), e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        M.T(getActivity(), getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }

    private void getDuplikasi() {
        //M.showLoadingDialog(getActivity());
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.get_duplikasi("getPegawaiTingkat1DanSekdaAsisten",
                M.getToken(getActivity()),
                St.getInstance().getLogin().getData().getPegawaiId(),
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, Response response) {
                        //M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");
                            mItems = new ArrayList<PegawaiDuplikasiModel>();
                            if (success == 1) {
                                JSONArray hasils = data.getJSONArray("pegawais");
                                for (int i = 0; i < hasils.length(); i++) {
                                    PegawaiDuplikasiModel tmp = new PegawaiDuplikasiModel();
                                    tmp.setId(hasils.getJSONObject(i).getString("id"));
                                    StringTokenizer tokens = new StringTokenizer(hasils.getJSONObject(i).getString("pegawai"), "-");
                                    String nama = tokens.nextToken();
                                    String jabatan = tokens.nextToken();
                                    tmp.setNama(nama.trim());
                                    tmp.setJabatan(jabatan.trim());
                                    tmp.setChecked(false);
                                    mItems.add(tmp);
                                }
//                                M.T(getActivity(), ""+mItems.size());
                            }
                            for (PegawaiDuplikasiModel c : mItems) {
                                for (int i = 0; i < selectedPegawaiIdTag.size(); i++) {
                                    if (c.getId().equalsIgnoreCase(selectedPegawaiIdTag.get(i))) {
                                        c.setChecked(true);
                                    }
                                }
                            }
                            mAdapter.setItems(mItems);
                        } catch (Exception e) {
                            //M.T(getActivity(), e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        //M.hideLoadingDialog();
                        M.T(getActivity(), getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }

    private void getSortDuplikasi(final String search) {
//        M.showLoadingDialog(getActivity());
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.get_duplikasi("getPegawaiTingkat1DanSekdaAsisten",
                M.getToken(getActivity()),
                St.getInstance().getLogin().getData().getPegawaiId(),
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, Response response) {
//                        M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");
                            mItems = new ArrayList<PegawaiDuplikasiModel>();
                            if (success == 1) {
                                JSONArray hasils = data.getJSONArray("pegawais");
                                for (int i = 0; i < hasils.length(); i++) {
                                    PegawaiDuplikasiModel tmp = new PegawaiDuplikasiModel();
                                    StringTokenizer tokens = new StringTokenizer(hasils.getJSONObject(i).getString("pegawai"), "-");
                                    String nama = tokens.nextToken();
                                    String jabatan = tokens.nextToken();
                                    if (hasils.getJSONObject(i).getString("pegawai").toLowerCase().contains(search)) {
                                        tmp.setId(hasils.getJSONObject(i).getString("id"));
                                        tmp.setNama(nama.trim());
                                        tmp.setJabatan(jabatan.trim());
                                        tmp.setChecked(false);
                                        mItems.add(tmp);
                                    }
                                }
                            }
                            for (PegawaiDuplikasiModel c : mItems) {
                                for (int i = 0; i < selectedPegawaiIdTag.size(); i++) {
                                    if (c.getId().equalsIgnoreCase(selectedPegawaiIdTag.get(i))) {
                                        c.setChecked(true);
                                    }
                                }
                            }
                            mAdapter.setItems(mItems);
                        } catch (Exception e) {
                            //M.T(getActivity(), e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        M.T(getActivity(), getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }

    //    private void getMasterAktivitas(final String bulan) {
//        //M.showLoadingDialog(getActivity());
//        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
//        api.get_master_aktivitas_struktural("getDaftarAktivitasStruktural",
//                M.getToken(getActivity()),
//                new Callback<Response>() {
//                    @Override
//                    public void success(Response resultx, Response response) {
//                        M.hideLoadingDialog();
//                        //
//                        BufferedReader reader = null;
//                        StringBuilder sb = new StringBuilder();
//                        try {
//                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
//                            String line;
//                            try {
//                                while ((line = reader.readLine()) != null) {
//                                    sb.append(line);
//                                }
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        String res = sb.toString();
//                        try {
//                            JSONObject obj = new JSONObject(res);
//                            JSONObject data = obj.getJSONObject("data");
//                            Integer success = data.getInt("success");
//                            master_aktivitas = new ArrayList<MasterAktivitasModel>();
//                            if (success == 1) {
//                                JSONArray hasils = data.getJSONArray("daftarAktivitas");
//                                for (int i = 0; i < hasils.length(); i++) {
//                                    MasterAktivitasModel tmp = new MasterAktivitasModel();
//                                    tmp.setNama(hasils.getString(i));
//                                    master_aktivitas.add(tmp);
//                                }
//                            }
//                            getKegiatan(bulan);
//                        } catch (Exception e) {
//                            M.T(getActivity(), e.getMessage());
//                        }
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error) {
//                        M.hideLoadingDialog();
//                        M.T(getActivity(), getString(R.string.ServerError));
//                        System.out.println("ERR: " + error.getMessage());
//                    }
//                });
//    }
    private void getMasterAktivitas(final String bulan) {
        M.showLoadingDialog(getActivity());
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.get_master_aktivitas("getDaftarAktivitasStruktural", M.getToken(getActivity()),
                St.getInstance().getLogin().getData().getPegawaiId(),
                bulan,
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, Response response) {
                        M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");
                            master_aktivitas = new ArrayList<MasterAktivitasModel>();
                            if (success == 1) {
                                JSONArray hasils = data.getJSONArray("daftarAktivitas");
                                for (int i = 0; i < hasils.length(); i++) {
                                    MasterAktivitasModel tmp = new MasterAktivitasModel();
                                    tmp.setAktivitasId(hasils.getJSONObject(i).getString("aktivitasId"));
                                    tmp.setNama(hasils.getJSONObject(i).getString("nama"));
                                    tmp.setBeban(hasils.getJSONObject(i).getString("beban"));
                                    master_aktivitas.add(tmp);
                                }
                            }
                            getKegiatan(bulan);
                        } catch (Exception e) {
                            M.T(getActivity(), e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        M.T(getActivity(), getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }

    private void cekCapaian(String tanggal) {
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.cek_capaian("cekCapaianHarian",
                M.getToken(getActivity()), tanggal, St.getInstance().getLogin().getData().getPegawaiId(),
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, Response response) {
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Boolean success = data.getBoolean("success");
                            capaian = success;
                            if (capaian == true) {
                                btnSave.setEnabled(true);
                            } else {
                                btnSave.setEnabled(false);
                                M.T(getActivity(), data.getString("msg"));
                            }
                        } catch (Exception e) {
                            M.T(getActivity(), e.getMessage());
                            capaian = false;
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.T(getActivity(), getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                        capaian = false;
                    }
                });
    }

    public void viewTag(LayoutInflater inflater) {
        dialogView = inflater.inflate(R.layout.dialog_tagging, null);
        rowView = inflater.inflate(R.layout.rowlayout, null);

        mList = (RecyclerView) dialogView.findViewById(R.id.listTag);
        find = (EditText) dialogView.findViewById(R.id.search_tag);
        check = (CheckBox) rowView.findViewById(R.id.chkTag);

        getDuplikasi();
        find.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                for (PegawaiDuplikasiModel pegawaiDuplikasiModel : mItems) {
                    try {
                        if (pegawaiDuplikasiModel.isChecked()) {
                            if (selectedPegawaiIdTag.contains(pegawaiDuplikasiModel.getId())) {
                                // if the item is already in the array, don't add

                            } else {
                                selectedPegawaiIdTag.add(pegawaiDuplikasiModel.getId());
                                selectedPegawaiNamaTag.add(pegawaiDuplikasiModel.getNama());
                            }
                        } else if (!pegawaiDuplikasiModel.isChecked()) {
                            // Else, if the item unchecked, remove it
                            selectedPegawaiIdTag.remove(pegawaiDuplikasiModel.getId());
                            selectedPegawaiNamaTag.remove(pegawaiDuplikasiModel.getNama());
                        }
                    } catch (Exception ex) {
                    }
                }
            }

            public void beforeTextChanged(CharSequence s,
                                          int start, int count, int after) {
                for (PegawaiDuplikasiModel pegawaiDuplikasiModel : mItems) {
                    try {
                        if (pegawaiDuplikasiModel.isChecked()) {
                            if (selectedPegawaiIdTag.contains(pegawaiDuplikasiModel.getId())) {
                                // if the item is already in the array, don't add

                            } else {
                                selectedPegawaiIdTag.add(pegawaiDuplikasiModel.getId());
                                selectedPegawaiNamaTag.add(pegawaiDuplikasiModel.getNama());
                            }
                        } else if (!pegawaiDuplikasiModel.isChecked()) {
                            // Else, if the item unchecked, remove it
                            selectedPegawaiIdTag.remove(pegawaiDuplikasiModel.getId());
                            selectedPegawaiNamaTag.remove(pegawaiDuplikasiModel.getNama());
                        }
                    } catch (Exception ex) {
                    }
                }
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                find.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                getSortDuplikasi(find.getText().toString().toLowerCase().trim());
            }
        });

        //layout manager
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mAdapter = new TaggingAdapter(getActivity(), mItems);
        //getDuplikasi();
        mList.setLayoutManager(layoutManager);
        mList.setAdapter(mAdapter);//set the adapter
    }

    public void initializeView() {
        myView = mView.findViewById(R.id.my_view);
        showMore = (TextView) mView.findViewById(R.id.textClick);

        myView.setVisibility(View.INVISIBLE);
        isShow = false;

        layoutPercent = (LinearLayout) getActivity().findViewById(R.id.layout_persentase);
        lblPercent = (TextView) getActivity().findViewById(R.id.lbl_persentase);
        jmlPercent = (TextView) getActivity().findViewById(R.id.total_persentase);
        percent = (ProgressBar) getActivity().findViewById(R.id.persentase);
        layoutPercent.setVisibility(View.VISIBLE);
        Calendar c = Calendar.getInstance();
        String[] monthName={"Januari","Februari","Maret", "April", "Mei", "Juni", "Juli",
                "Agustus", "September", "Oktober", "November",
                "Desember"};
        getRealisasi(String.valueOf((c.get(Calendar.MONTH) + 1)));
        lblPercent.setText("Capaian Aktivitas Bulan " + monthName[c.get(Calendar.MONTH)]);

        btnSave = (Button) mView.findViewById(R.id.btnSave);
        btnSave2 = (Button) mView.findViewById(R.id.btnSave2);

        btnPilihGambar = (ImageButton) mView.findViewById(R.id.btnPilihGambar);
        lblPilihGambar = (TextView) mView.findViewById(R.id.lblPilihGambar);
        btnSinkron = (ImageButton) mView.findViewById(R.id.sinkron);
        lokasilbl = (TextView) mView.findViewById(R.id.lokasi_lbl);
        lampiran = (ImageView) mView.findViewById(R.id.gambarPendukung);
//        Zoomy.Builder builder = new Zoomy.Builder(getActivity()).target(lampiran);
//        builder.register();

        aktivitas = (EditText) mView.findViewById(R.id.aktivitas);
        tag = (EditText) mView.findViewById(R.id.duplikasi);
        lblDuplikasi = (TextView) mView.findViewById(R.id.lblduplikasi);
        kegiatan = (EditText) mView.findViewById(R.id.kegiatan);
        catatan = (EditText) mView.findViewById(R.id.catatan);
//        satuan_output = (EditText) mView.findViewById(R.id.satuanOutput);
//        jumlah_output = (EditText) mView.findViewById(R.id.jmlOutput);
//        menit_waktu_pengerjaan = (EditText) mView.findViewById(R.id.waktuPengerjaan);

        rg = (RadioGroup) mView.findViewById(R.id.radioGroupAktivitas);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbJamKerja:
                        // do operations specific to this selection
                        jenis_waktu_pengerjaan = 0;
                        break;
                    case R.id.rbLuarJamKerja:
                        // do operations specific to this selection
                        jenis_waktu_pengerjaan = 1;
                        break;
                }
            }
        });
        tanggal = (EditText) mView.findViewById(R.id.tanggal);
        tanggal.setOnClickListener(this);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        ymdFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        bulanFormatter = new SimpleDateFormat("MM", Locale.US);
        Date date = new Date();
//        tanggal.setText(dateFormatter.format(date));
        setbulan = bulanFormatter.format(date).toString();
        tanggal_ymd = ymdFormatter.format(date);
        Calendar newCalendar = Calendar.getInstance();
        tanggalDlg = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tanggal.setText(dateFormatter.format(newDate.getTime()));
                setbulan = bulanFormatter.format(newDate.getTime()).toString();
                tanggal_ymd = ymdFormatter.format(newDate.getTime());
                cekCapaian(ymdFormatter.format(newDate.getTime()));
                getMasterAktivitas(bulanFormatter.format(newDate.getTime()).toString());
                enableEntry();
                System.out.println("" + bulanFormatter.format(newDate.getTime()).toString());
                //getKegiatan(bulanFormatter.format(newDate.getTime()).toString());
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        aktivitas.setOnClickListener(this);
        kegiatan.setOnClickListener(this);
        tag.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnSave2.setOnClickListener(this);
        btnSave2.setVisibility(View.INVISIBLE);
        btnPilihGambar.setOnClickListener(this);
        lblPilihGambar.setOnClickListener(this);
        btnSinkron.setOnClickListener(this);
        lampiran.setOnClickListener(this);
        if (St.getInstance().getLogin().getData().isCamat() == true) {
            lblDuplikasi.setVisibility(View.INVISIBLE);
            tag.setVisibility(View.INVISIBLE);
        }

        //
        getDuplikasi();
//        getMasterAktivitas(bulanFormatter.format(date).toString());
        disableEntry();
    }

    private void doSimpan1() {
        final String[] kegiatan_ids;
        kegiatan_ids = new String[kegiatan_id.size()];
        for (int i = 0; i < kegiatan_id.size(); i++) {
            kegiatan_ids[i] = kegiatan_id.get(i) + "#" + kegiatan_jenis.get(i);
        }
        String status = St.getInstance().getAktif_status();
        String id = null;
        M.showLoadingDialog(getActivity());
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.aktivitas_simpan(dataSimpan1("doSimpanAktivitasPegawai".toString(), M.getToken(getActivity()).toString(),
                St.getInstance().getLogin().getData().getPegawaiId().toString(), tanggal_ymd.toString(),
                aktivitas_string.toString(), String.valueOf(jenis_waktu_pengerjaan), Arrays.asList(kegiatan_ids),
                catatan.getText().toString().trim(), lokasi_koordinat.toString(),
                lokasi_alamat.toString(), imagePath), new Callback<Response>() {
            @Override
            public void success(Response resultx, retrofit.client.Response response) {
                M.hideLoadingDialog();
                //
                BufferedReader reader = null;
                StringBuilder sb = new StringBuilder();
                try {
                    reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                    String line;
                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String res = sb.toString();
                try {
                    JSONObject obj = new JSONObject(res);
                    JSONObject data = obj.getJSONObject("data");
                    Integer success = data.getInt("success");
                    String msg = data.getString("msg");
                    M.T(getActivity(), msg);
                    if (success == 1) {
                        tanggal.setError(null);
                        aktivitas.setText("");
                        aktivitas_string = "";
//                        menit_waktu_pengerjaan.setText("");
//                                if(!realPath.isEmpty()){
//                                    realPath.clear();
//                                }
                        if (imageUri != null) {
                            imageUri = null;
                        }
                        gps.getLocation();
                        sinkronLokasi();
                        imagePath = null;
                        if (lampiran.getDrawable() != null) {
                            lampiran.setImageResource(0);
                        }
                        //updateImageList();
                    } else {
                        tanggal.setError("tanggal invalid");
                    }
                } catch (Exception e) {
                    M.T(getActivity(), e.getMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                M.T(getActivity(), getString(R.string.ServerError));
                System.out.println("ERR: " + error.getMessage());
            }
        });
    }


    private void doSimpan2() {
        final String[] kegiatan_ids;
        kegiatan_ids = new String[kegiatan_id.size()];
        for (int i = 0; i < kegiatan_id.size(); i++) {
            kegiatan_ids[i] = kegiatan_id.get(i) + "#" + kegiatan_jenis.get(i);
        }
        String status = St.getInstance().getAktif_status();
        String id = null;

        M.showLoadingDialog(getActivity());
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.aktivitas_simpan(dataSimpan2("doSimpanAktivitasPegawai".toString(), M.getToken(getActivity()).toString(),
                St.getInstance().getLogin().getData().getPegawaiId().toString(), tanggal_ymd.toString(),
                aktivitas_string.toString(), String.valueOf(jenis_waktu_pengerjaan), "",
                "", Arrays.asList(kegiatan_ids), catatan.getText().toString().trim(),
                selectedPegawaiIdTag, "", lokasi_koordinat.toString(),
                lokasi_alamat.toString(), imagePath), new Callback<Response>() {
            @Override
            public void success(Response resultx, retrofit.client.Response response) {
                M.hideLoadingDialog();
                //
                BufferedReader reader = null;
                StringBuilder sb = new StringBuilder();
                try {
                    reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                    String line;
                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String res = sb.toString();
                try {
                    JSONObject obj = new JSONObject(res);
                    JSONObject data = obj.getJSONObject("data");
                    Integer success = data.getInt("success");
                    String msg = data.getString("msg");
                    M.T(getActivity(), msg);
                    if (success == 1) {
                        clearData();
                    } else {
                        tanggal.setError("tanggal invalid");
                    }
                } catch (Exception e) {
                    M.T(getActivity(), e.getMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                M.T(getActivity(), getString(R.string.ServerError));
                System.out.println("ERR: " + error.getMessage());
            }
        });
    }

    private void clearData() {
        tanggal.setError(null);
        aktivitas.setText("");
        aktivitas_string = "";
//        menit_waktu_pengerjaan.setText("");
//        jumlah_output.setText("");
//        satuan_output.setText("");
        kegiatan.setText("");
        catatan.setText("");
        tag.setText("");
        selectedPegawaiNamaTag.clear();
        selectedPegawaiIdTag.clear();
        kegiatan_id.clear();
        kegiatan_nama.clear();
        kegiatan_jenis.clear();
//        if(!realPath.isEmpty()){
//            realPath.clear();
//        }
        if (imageUri != null) {
            imageUri = null;
        }
        gps.getLocation();
        sinkronLokasi();
        imagePath = null;
        if (lampiran.getDrawable() != null) {
            lampiran.setImageResource(0);
        }
        //updateImageList();
        disableEntry();
    }

    private void disableEntry() {
        aktivitas.setEnabled(false);
        kegiatan.setEnabled(false);
        catatan.setEnabled(false);
    }

    private void enableEntry() {
        aktivitas.setEnabled(true);
        kegiatan.setEnabled(true);
        catatan.setEnabled(true);
    }

    private ArrayList<String> array_sort;
    private String ars_aktivitas[];
    private String ars_duplikasi[];

    @Override
    public void onClick(View v) {
        AlertDialog alertCat;
        List<String> lst = new ArrayList<>();
        List<String> id = new ArrayList<>();
        List<String> kegJenis = new ArrayList<>();
        List<Boolean> bol = new ArrayList<>();
        final String[] jenis;
        final String[] ars;
        final String[] ids;
        final boolean[] checked;
        if (v.getId() == R.id.btnSave) {
            if (St.getInstance().getLogin().getData().getJabatan().equalsIgnoreCase("Jabatan Belum diset")) {
                Toast.makeText(getActivity(), "Jabatan anda belum disetting, setting jabatan anda terlebih dahulu sebelum melakukan entry aktivitas",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (aktivitas.getText().toString().equalsIgnoreCase("") || aktivitas_string.equalsIgnoreCase("") || aktivitas_string == "") {
                aktivitas.setError("Pilih aktivitas");
                Toast.makeText(getActivity(), "Pilih aktivitas",
                        Toast.LENGTH_LONG).show();
                return;
            }
//            else if (menit_waktu_pengerjaan.getText().toString().equalsIgnoreCase("") || Integer.parseInt(menit_waktu_pengerjaan.getText().toString()) < 5) {
//                menit_waktu_pengerjaan.setError("Isi waktu pengerjaan minimal 5 menit");
//                Toast.makeText(getActivity(), "Isi waktu pengerjaan minimal 5 menit",
//                        Toast.LENGTH_LONG).show();
//                return;
//            }
            else if (!gps.canGetLocation()) {
                Toast.makeText(getActivity(), "Gagal mengambil info lokasi, aktifkan GPS terlebih dahulu sebelum entry aktivitas",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (imagePath == null) {
                Toast.makeText(getActivity(), "Unggah data pendukung terlebih dahulu !",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (lokasi_alamat == null || lokasi_alamat.equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "Tidak dapat menemukan lokasi entry aktivitas, silahkan coba lagi",
                        Toast.LENGTH_LONG).show();
                return;
            }

//            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2){
//                if(gps.isMockLocation(lokasi)){
//                    Toast.makeText(getActivity(), "Terdeteksi adanya penggunaan lokasi palsu pada perangkat anda, silahkan dinonaktifkan terlebih dahulu kemudian restart device anda.",
//                            Toast.LENGTH_LONG).show();
//                    return;
//                }
//            } else {
//                if(gps.isMockLocationEnabled(getActivity())){
//                    Toast.makeText(getActivity(), "Terdeteksi adanya penggunaan lokasi palsu pada perangkat anda, silahkan dinonaktifkan terlebih dahulu kemudian restart device anda.",
//                            Toast.LENGTH_LONG).show();
//                    return;
//                }
//            }

            doSimpan1();
        } else if (v.getId() == R.id.btnSave2) {
            if (St.getInstance().getLogin().getData().getJabatan().equalsIgnoreCase("Jabatan Belum diset")) {
                Toast.makeText(getActivity(), "Jabatan anda belum disetting, setting jabatan anda terlebih dahulu sebelum melakukan entry aktivitas",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (aktivitas.getText().toString().equalsIgnoreCase("") || aktivitas_string.equalsIgnoreCase("") || aktivitas_string == "") {
                aktivitas.setError("Pilih aktivitas");
                Toast.makeText(getActivity(), "Pilih aktivitas",
                        Toast.LENGTH_LONG).show();
                return;
            }
//            else if (menit_waktu_pengerjaan.getText().toString().equalsIgnoreCase("") || Integer.parseInt(menit_waktu_pengerjaan.getText().toString()) < 5) {
//                menit_waktu_pengerjaan.setError("Isi waktu pengerjaan minimal 5 menit");
//                Toast.makeText(getActivity(), "Isi waktu pengerjaan minimal 5 menit",
//                        Toast.LENGTH_LONG).show();
//                return;
//            }
            else if (gps.canGetLocation() == false) {
                Toast.makeText(getActivity(), "Gagal mengambil info lokasi, aktifkan GPS terlebih dahulu sebelum entry aktivitas",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (imagePath == null) {
                Toast.makeText(getActivity(), "Unggah data pendukung terlebih dahulu !",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (lokasi_alamat == null || lokasi_alamat.equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "Tidak dapat menemukan lokasi entry aktivitas, silahkan coba lagi",
                        Toast.LENGTH_LONG).show();
                return;
            }

//            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2){
//                if(gps.isMockLocation(lokasi)){
//                    Toast.makeText(getActivity(), "Terdeteksi adanya penggunaan lokasi palsu pada perangkat anda, silahkan dinonaktifkan terlebih dahulu kemudian restart device anda.",
//                            Toast.LENGTH_LONG).show();
//                    return;
//                }
//            } else {
//                if(gps.isMockLocationEnabled(getActivity())){
//                    Toast.makeText(getActivity(), "Terdeteksi adanya penggunaan lokasi palsu pada perangkat anda, silahkan dinonaktifkan terlebih dahulu kemudian restart device anda.",
//                            Toast.LENGTH_LONG).show();
//                    return;
//                }
//            }

            if (kegiatan_id.size() > 1) {
                android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
                //alertDialogBuilder.setMessage("Waktu Pelaksanaan = " + Integer.parseInt(menit_waktu_pengerjaan.getText().toString()) / kegiatan_id.size() + " menit, didapat dari " + menit_waktu_pengerjaan.getText().toString() + "/" + kegiatan_id.size() + " (jumlah kegiatan terpilih).");
                alertDialogBuilder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                doSimpan2();
                            }
                        });

                alertDialogBuilder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                            }
                        });

                //Showing the alert dialog
                android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            } else {
                doSimpan2();
            }
        } else if (v.getId() == R.id.tanggal) {
            long now = System.currentTimeMillis() - 1000;
            if (St.getInstance().getLogin().getData().isKunciInputHariLalu()) {
                tanggalDlg.getDatePicker().setMinDate(now - (1000 * 60 * 60 * 24 * 6));
            }
            tanggalDlg.getDatePicker().setMaxDate(new Date().getTime());
            tanggalDlg.setInverseBackgroundForced(true);
            tanggalDlg.show();
        } else if (v.getId() == R.id.aktivitas) {
            if (master_aktivitas == null) {
                aktivitas.setError("Gagal ambil info aktivitas");
                return;
            }
            for (MasterAktivitasModel s : master_aktivitas) {
                lst.add(s.getNama());
            }
            ars = lst.toArray(new String[lst.size()]);
            ars_aktivitas = ars;
            builderAkt.setTitle("Pilih aktivitas");
            do_aktivitas();

        } else if (v.getId() == R.id.kegiatan) {
            if (kegiatans == null) {
                kegiatan.setError("Gagal ambil info kegiatan");
                return;
            }

            for (PenugasanKegiatanModel s : kegiatans) {
                id.add(s.getId());
                lst.add(s.getNama());
                kegJenis.add(s.getJenis());
                bol.add(s.isStatus());
            }
            ids = id.toArray(new String[id.size()]);
            ars = lst.toArray(new String[lst.size()]);
            jenis = kegJenis.toArray(new String[kegJenis.size()]);
            checked = new boolean[bol.size()];
            for (int i = 0; i < bol.size(); i++) {
                checked[i] = bol.get(i);
            }
            builderKeg.setTitle("Pilih kegiatan");
            builderKeg.setMultiChoiceItems(ars, checked, new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                    if (isChecked) {
                        // If the user checked the item, add it to the selected items
                        if (kegiatan_id.contains(ids[indexSelected])) {
                            // Else, if the item is already in the array, remove it
                            kegiatan_id.remove(ids[indexSelected]);
                            kegiatan_nama.remove(ars[indexSelected]);
                            kegiatan_jenis.remove(jenis[indexSelected]);
                        }
                        kegiatan_id.add(ids[indexSelected]);
                        kegiatan_nama.add(ars[indexSelected]);
                        kegiatan_jenis.add(jenis[indexSelected]);
                    } else if (!isChecked) {
                        // Else, if the item unchecked, remove it
                        kegiatan_id.remove(ids[indexSelected]);
                        kegiatan_nama.remove(ars[indexSelected]);
                        kegiatan_jenis.remove(jenis[indexSelected]);
                    }
                }
            });
            builderKeg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    //  Your code when user clicked on OK
                    String str = TextUtils.join(" - ", kegiatan_nama);
//                    if (str.equalsIgnoreCase("none"))
//                        str = "";
                    kegiatan.setText(str);
                    getKegiatan(setbulan);
                }
            });
            builderKeg.setCancelable(false);
            builderKeg.setInverseBackgroundForced(true);

            alertCat = builderKeg.create();
            alertCat.show();
        } else if (v.getId() == R.id.duplikasi) {
            if (mItems == null) {
                tag.setError("Gagal ambil info pegawai");
                return;
            }
            for (PegawaiDuplikasiModel s : mItems) {
                lst.add(s.getNama());
            }
            ars = lst.toArray(new String[lst.size()]);
            ars_duplikasi = ars;

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            viewTag(inflater);
            builderPeg.setView(dialogView);
            builderPeg.setTitle("Pilih Pegawai");
            builderPeg.setCancelable(false);
            builderPeg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    try {
                        for (PegawaiDuplikasiModel pegawaiDuplikasiModel : mItems) {
                            try {
                                if (pegawaiDuplikasiModel.isChecked()) {
                                    if (selectedPegawaiIdTag.contains(pegawaiDuplikasiModel.getId())) {
                                        // if the item is already in the array, don't add

                                    } else {
                                        selectedPegawaiIdTag.add(pegawaiDuplikasiModel.getId());
                                        selectedPegawaiNamaTag.add(pegawaiDuplikasiModel.getNama());
                                    }
                                } else if (!pegawaiDuplikasiModel.isChecked()) {
                                    // Else, if the item unchecked, remove it
                                    selectedPegawaiIdTag.remove(pegawaiDuplikasiModel.getId());
                                    selectedPegawaiNamaTag.remove(pegawaiDuplikasiModel.getNama());
                                }
                            } catch (Exception ex) {
                            }
                        }
                        String str = TextUtils.join(" - ", selectedPegawaiNamaTag);
                        if (str.equalsIgnoreCase("none") || str.equalsIgnoreCase(null) || str.equalsIgnoreCase("null"))
                            str = "";
                        tag.setText(str);
                    } catch (Exception err) {

                    }
                }
            });

            builderPeg.setInverseBackgroundForced(true);
            alertCat = builderPeg.create();
            alertCat.show();
        } else if (v.getId() == R.id.btnPilihGambar) {
            if (!permissions.checkPermissionCameraStorage(getActivity())) {
                permissions.setPermissionCameraStorage(getActivity());
            } else if (!permissions.checkPermissionCamera(getActivity())) {
                permissions.setPermissionCamera(getActivity());
            } else if (!permissions.checkPermissionStorage(getActivity())) {
                permissions.setPermissionStorage(getActivity());
            } else {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(getActivity());
                startActivityForResult(chooseImageIntent, REQUEST_IMAGE_PICK);
            }
        } else if (v.getId() == R.id.lblPilihGambar) {
            if (!permissions.checkPermissionCameraStorage(getActivity())) {
                permissions.setPermissionCameraStorage(getActivity());
            } else if (!permissions.checkPermissionCamera(getActivity())) {
                permissions.setPermissionCamera(getActivity());
            } else if (!permissions.checkPermissionStorage(getActivity())) {
                permissions.setPermissionStorage(getActivity());
            } else {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(getActivity());
                startActivityForResult(chooseImageIntent, REQUEST_IMAGE_PICK);
            }
        } else if (v.getId() == R.id.sinkron) {
            if (permissions.checkPermissionLocation(getActivity())) {
                if (gps.isLocationEnabled(getActivity())) {
                    gps.getLocation();
                    M.showLoadingDialog(getActivity());
                    sinkronLokasi();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            M.hideLoadingDialog();
                        }
                    }, 1500);
                } else {
                    gps.showSettingsAlert();
                }
            } else {
                permissions.setPermissionLocation(getActivity());
            }
        } else if (v.getId() == R.id.gambarPendukung) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater factory = LayoutInflater.from(getActivity());
            View mView = factory.inflate(R.layout.dialog_zoom_image, null);
            pinchZoomImageView = (PinchZoomImageView) mView.findViewById(R.id.pinchZoomImageView);
            pinchZoomImageView.setImageUri(imageUri);
            dialogBuilder.setView(mView);
            AlertDialog dialog = dialogBuilder.create();
            dialog.show();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQUEST_IMAGE_PICK:
                Bitmap bitmap = ImagePicker.getImageFromResult(getActivity(), resultCode, data);
                if (bitmap != null) {
                    lampiran.setImageBitmap(bitmap);
                    File file = ImagePicker.createImageFile(getActivity());
                    imageUri = ImagePicker.getUriFromFile(file, bitmap);
                    imagePath = FileUtils.getPath(getActivity(), imageUri);
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }

    }

    private MultipartTypedOutput dataSimpan1(String action, String token, String idPegawai, String tanggal,
                                             String stringAktivitas, String jenisWaktuPengerjaan, List<String> kegiatan_ids,
                                             String catatan, String koordinat, String lokasi, String path) {
        MultipartTypedOutput multipartTypedOutput = new MultipartTypedOutput();
        multipartTypedOutput.addPart("obj[action]", new TypedString(action));
        multipartTypedOutput.addPart("obj[token]", new TypedString(token));
        multipartTypedOutput.addPart("obj[pegawai_id]", new TypedString(idPegawai));
        multipartTypedOutput.addPart("obj[tanggal]", new TypedString(tanggal));
        if (!kegiatan_ids.isEmpty()) {
            for (int i = 0; i < kegiatan_ids.size(); i++) {
                multipartTypedOutput.addPart("obj[kegiatan_ids][]", new TypedString(kegiatan_ids.get(i)));
            }
        }
//        multipartTypedOutput.addPart("obj[aktivitas_string]", new TypedString(stringAktivitas));
        multipartTypedOutput.addPart("obj[aktivitas_id]", new TypedString(stringAktivitas));
        multipartTypedOutput.addPart("obj[catatan]", new TypedString(catatan));
        multipartTypedOutput.addPart("obj[jenis_waktu_pengerjaan]", new TypedString(jenisWaktuPengerjaan));

        multipartTypedOutput.addPart("obj[lokasi_koordinat]", new TypedString(koordinat));
        multipartTypedOutput.addPart("obj[lokasi_alamat]", new TypedString(lokasi));
        multipartTypedOutput.addPart("files[]", new TypedFile("image/jpeg", new File(path)));
//        if(path.isEmpty()){
//
//        } else {
//            for (int i = 0; i < path.size(); i++) {
//                multipartTypedOutput.addPart("files[]", new TypedFile("image/jpeg", new File(path.get(i))));
//            }
//        }

        return multipartTypedOutput;
    }

    private MultipartTypedOutput dataSimpan2(String action, String token, String idPegawai, String tanggal,
                                             String stringAktivitas, String jenisWaktuPengerjaan, String jumlahOutput,
                                             String satuanOutput, List<String> kegiatan_ids, String catatan, List<String> setara_ids,
                                             String menitWaktuPengerjaan, String koordinat, String lokasi, String path) {
        MultipartTypedOutput multipartTypedOutput = new MultipartTypedOutput();
        multipartTypedOutput.addPart("obj[action]", new TypedString(action));
        multipartTypedOutput.addPart("obj[token]", new TypedString(token));
        multipartTypedOutput.addPart("obj[pegawai_id]", new TypedString(idPegawai));
        multipartTypedOutput.addPart("obj[tanggal]", new TypedString(tanggal));
        if (!kegiatan_ids.isEmpty()) {
            for (int i = 0; i < kegiatan_ids.size(); i++) {
                multipartTypedOutput.addPart("obj[kegiatan_ids][]", new TypedString(kegiatan_ids.get(i)));
            }
        }
//        multipartTypedOutput.addPart("obj[aktivitas_string]", new TypedString(stringAktivitas));
        multipartTypedOutput.addPart("obj[aktivitas_id]", new TypedString(stringAktivitas));
        multipartTypedOutput.addPart("obj[catatan]", new TypedString(catatan));
        multipartTypedOutput.addPart("obj[jenis_waktu_pengerjaan]", new TypedString(jenisWaktuPengerjaan));

        multipartTypedOutput.addPart("obj[lokasi_koordinat]", new TypedString(koordinat));
        multipartTypedOutput.addPart("obj[lokasi_alamat]", new TypedString(lokasi));
        multipartTypedOutput.addPart("files[]", new TypedFile("image/jpeg", new File(path)));
        //        multipartTypedOutput.addPart("obj[jumlah_output]", new TypedString(jumlahOutput));
//        multipartTypedOutput.addPart("obj[satuan_output]", new TypedString(satuanOutput));
        //        multipartTypedOutput.addPart("obj[menit_waktu_pengerjaan]", new TypedString(menitWaktuPengerjaan));
        if (!setara_ids.isEmpty()) {
            for (int i = 0; i < setara_ids.size(); i++) {
                multipartTypedOutput.addPart("obj[setara_ids][]", new TypedString(setara_ids.get(i)));
            }
        }
//        if(path.isEmpty()){
//
//        } else {
//            for (int i = 0; i < path.size(); i++) {
//                multipartTypedOutput.addPart("files[]", new TypedFile("image/jpeg", new File(path.get(i))));
//            }
//        }

        return multipartTypedOutput;
    }

    int textlength = 0;
    private AlertDialog myalertDialog = null;

    void do_aktivitas() {
        final EditText editText = new EditText(this.getActivity());
        final ListView listview = new ListView(this.getActivity());
        editText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_search, 0, 0, 0);
        array_sort = new ArrayList<String>(Arrays.asList(ars_aktivitas));
        LinearLayout layout = new LinearLayout(this.getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.addView(editText);
        layout.addView(listview);
        builderAkt.setView(layout);
        CustomAlertAdapter arrayAdapter = new CustomAlertAdapter(this.getActivity(), array_sort);
        listview.setAdapter(arrayAdapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                //alertCat.dismiss();
                myalertDialog.dismiss();
                //String str = ars_aktivitas[position];
                String str = array_sort.get(position).toString();
                if (str.equalsIgnoreCase("none"))
                    str = "";
                aktivitas.setText(str);
                for (MasterAktivitasModel s : master_aktivitas) {
                    if (s.getNama().equalsIgnoreCase(str)) {
//                        aktivitas_string = s.getNama();
                        aktivitas_string = s.getAktivitasId();
                        break;
                    }
                }
            }
        });
        editText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s,
                                          int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                textlength = editText.getText().length();
                array_sort.clear();
                for (int i = 0; i < ars_aktivitas.length; i++) {
                    if (textlength <= ars_aktivitas[i].length()) {

                        if (ars_aktivitas[i].toLowerCase().contains(editText.getText().toString().toLowerCase().trim())) {
                            array_sort.add(ars_aktivitas[i]);
                        }
                    }
                }
                listview.setAdapter(new CustomAlertAdapter(AddAktivitasKapdAsistenSekdaFragment.this.getActivity(), array_sort));
            }
        });
//        builderAkt.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                aktivitas.setText(editText.getText().toString());
//                aktivitas_string = aktivitas.getText().toString();
//            }
//        });
//        builderAkt.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
        builderAkt.setInverseBackgroundForced(true);
        myalertDialog = builderAkt.show();
    }

    // slide the view from below itself to the current position
    public void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        btnSave.setVisibility(View.INVISIBLE);
        btnSave2.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {
        view.clearAnimation();
        view.setVisibility(View.INVISIBLE);
        btnSave2.setVisibility(View.INVISIBLE);
        btnSave.setVisibility(View.VISIBLE);
    }

    private void sinkronLokasi() {
        if (gps.canGetLocation()) {
            boolean mockLocation = false;
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                mockLocation = gps.getLocation().isFromMockProvider();
            } else {
                mockLocation = gps.isMockLocationOn(getActivity());
            }
            if (mockLocation) {
                gps.showFakeGpsAlert();
            } else {
                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();

                Geocoder geocoder;
                List<Address> addresses = null;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    if (addresses != null && addresses.size() > 0) {
                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName();

                        lokasilbl.setText(address);
                        lokasi_koordinat = "" + latitude + ", " + longitude;
                        lokasi_alamat = address;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            gps.showSettingsAlert();
        }
    }

}