package id.go.kotasurabaya.eperformance.api;

import android.util.Log;

import id.go.kotasurabaya.eperformance.app.AppConst;
import com.google.gson.annotations.SerializedName;
import id.go.kotasurabaya.eperformance.data.St;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;



public class APIService {
    public APIService() {

    }

    public static <S> S createService(final Class<S> serviceClass, final String token) {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(AppConst.MAIN)
                .setLogLevel(RestAdapter.LogLevel.FULL);
        builder.setRequestInterceptor(new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("token", token);
                //Log.d("createService", "t=" + token + "#");
                if(St.getInstance().getKoplak()!=null) {
                    Log.d(AppConst.TAG, "k=" + St.getInstance().getKoplak());
                    request.addHeader("Cookie", "BAMBANGTRIBASUKI=" + St.getInstance().getKoplak());
                }
            }
        });
        RestAdapter adapter = builder.build();
        //dbg
        //RestAdapter adapter = (builder.build()).setLogLevel(RestAdapter.LogLevel.FULL);

        return adapter.create(serviceClass);
    }

    public static <S> S createService(Class<S> serviceClass) {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(AppConst.MAIN);
        RestAdapter adapter = builder.build();
        //dbg
        //RestAdapter adapter = (builder.build()).setLogLevel(RestAdapter.LogLevel.FULL);
        //Log.d("createService","#"+adapter.toString());

        return adapter.create(serviceClass);
    }

    //@Parcel
    public class RestError
    {
        @SerializedName("code")
        private Integer code;

        @SerializedName("error_message")
        private String strMessage;

        public RestError(String strMessage)
        {
            this.strMessage = strMessage;
        }

        //Getters and setters
    }

}
