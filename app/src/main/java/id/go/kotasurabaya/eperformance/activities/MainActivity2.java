package id.go.kotasurabaya.eperformance.activities;

//import android.support.v4.app.Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.go.kotasurabaya.eperformance.R;
import id.go.kotasurabaya.eperformance.data.St;
import id.go.kotasurabaya.eperformance.fragments.AddAktivitasOsFragment;
import id.go.kotasurabaya.eperformance.fragments.AktivitasOsFragment;
import id.go.kotasurabaya.eperformance.helpers.AlertDialogManager;
import id.go.kotasurabaya.eperformance.helpers.CropSquareTransformation;
import id.go.kotasurabaya.eperformance.helpers.M;
import id.go.kotasurabaya.eperformance.helpers.Sess;
import me.leolin.shortcutbadger.ShortcutBadger;

public class MainActivity2 extends AppCompatActivity {
    public DrawerLayout mDrawerLayout;
    public NavigationView mNavigationView;
    private Toolbar toolbar;
    private FragmentManager mFragmentManager;
    private Fragment mFragment = null;
    private Intent mIntent = null;
    AsyncTask<Void, Void, Void> mRegisterTask;
    AlertDialogManager alert = new AlertDialogManager();
    LinearLayout layoutPercent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Sess sess = new Sess(this);
        setContentView(R.layout.activity_main);
        initializeView();
        St.getInstance().setMain2(this);
        Intent fromEdit = getIntent();
        Bundle b = fromEdit.getExtras();
        if(b!=null)
        {
            if(b.get("edit") == "success") {
                St.getInstance().setAktif_status("0");
                switchContent(new AktivitasOsFragment());
                mNavigationView.getMenu().findItem(R.id.menuAktivitasBaru).setChecked(true);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mNavigationView.getMenu().findItem(R.id.menuHomeItem).setVisible(false);
        mNavigationView.getMenu().findItem(R.id.menuAktivitasBawahan).setVisible(false);
    }

    public void initializeView() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.navigationView);
        mNavigationView.getMenu().findItem(R.id.menuHomeItem).setVisible(false);
        mNavigationView.getMenu().findItem(R.id.menuAktivitasBawahan).setVisible(false);
        layoutPercent = (LinearLayout) findViewById(R.id.layout_persentase);

        St.getInstance().setAktif_status("0");
        switchContent(new AktivitasOsFragment());
        mNavigationView.getMenu().findItem(R.id.menuAktivitasBaru).setChecked(true);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                displayView(menuItem.getItemId());
                menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
        updateDrawerHeaderInfo();
    }

    public void updateDrawerHeaderInfo() {
        updateHeaderView();
    }

    public void updateHeaderView() {

        ImageView profilePicture = (ImageView) findViewById(R.id.profilPic);
        Picasso.get()
                .load(R.drawable.logo_surabaya)
                .transform(new CropSquareTransformation())
                .placeholder(R.drawable.image_holder)
                .error(R.drawable.image_holder)
                .into(profilePicture);
        TextView userName = (TextView) findViewById(R.id.userName);
        TextView userJob = (TextView) findViewById(R.id.userJob);
        //ImageView profileCover = (ImageView) findViewById(R.id.profileCover);
        if(St.getInstance().getLoginOs()!=null) {
            if (St.getInstance().getLoginOs().getData() != null) {
                userJob.setText(St.getInstance().getLoginOs().getData().getJabatan());
                userName.setText(St.getInstance().getLoginOs().getData().getNama());
                Log.d("###", St.getInstance().getLoginOs().getData().getNama());

            }
        }

    }

    public void displayView(int id) {
        switch (id) {

            case R.id.menuAktivitasAdd:
                mFragment = new AddAktivitasOsFragment();
                break;
            case R.id.menuAktivitasBaru:
                layoutPercent.setVisibility(View.GONE);
                St.getInstance().setAktif_status("0");
                mFragment = new AktivitasOsFragment();
                break;
            case R.id.menuAktivitasTanpaFilePendukung:
                layoutPercent.setVisibility(View.GONE);
                St.getInstance().setAktif_status("102");
                mFragment = new AktivitasOsFragment();
                break;
            case R.id.menuAktivitasTelahDisahkan:
                layoutPercent.setVisibility(View.GONE);
                St.getInstance().setAktif_status("1");
                mFragment = new AktivitasOsFragment();
                break;
            case R.id.menuAktivitasRevisi:
                layoutPercent.setVisibility(View.GONE);
                St.getInstance().setAktif_status("100");
                mFragment = new AktivitasOsFragment();
                break;
            case R.id.menuAktivitasDitolak:
                layoutPercent.setVisibility(View.GONE);
                St.getInstance().setAktif_status("-1");
                mFragment = new AktivitasOsFragment();
                break;

            case R.id.logoutItem:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Apakah Anda Yakin Ingin Keluar ?");
                alertDialogBuilder.setPositiveButton("Ya",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                M.logOut(getApplicationContext());
                                //tess aj
                                //GCMRegistrar.checkDevice(this);
                                //GCMRegistrar.unregister(this);
                                //
                                ShortcutBadger.removeCount(getApplicationContext()); //for 1.1.4+
                                finish();
                                mIntent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(mIntent);
                            }
                        });

                alertDialogBuilder.setNegativeButton("Tidak",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                            }
                        });

                //Showing the alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                break;

            default:
                mFragment = new AktivitasOsFragment();
                break;
        }

        if (mFragment != null && mIntent == null) {
            //mFragmentManager = getFragmentManager();
            //St.getInstance().setFragmentManager(mFragmentManager);
            //mFragmentManager.beginTransaction().replace(R.id.frameContainer, mFragment).commit();
            switchContent(mFragment);
        } else {
            //startActivity(mIntent);
            //mIntent = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public void switchContent(Fragment fragment) {
        mFragment = fragment;
        //getSupportFragmentManager().beginTransaction().replace(R.id.frameContainer, fragment).commit(); //v4
        MainActivity2.this.getFragmentManager().beginTransaction().replace(R.id.frameContainer, fragment).commit();
        //slidemenu.showContent();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Apakah Anda Ingin Keluar Aplikasi ?");
        alertDialogBuilder.setPositiveButton("Ya",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                });

        alertDialogBuilder.setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        //Showing the alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ArrayList newMessage = new ArrayList<String>();
            newMessage = intent.getStringArrayListExtra("message");
            //Toast.makeText(getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();
        }
    };


}
