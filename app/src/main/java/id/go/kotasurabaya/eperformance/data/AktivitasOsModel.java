package id.go.kotasurabaya.eperformance.data;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AktivitasOsModel {
    int nomor = 0, id = 0, status = 0, aktivitasId = 0, aktivitasBeban = 0, kegiatanId = 0;
    String updatedAt, statusNama, aktivitasNama, aktivitasSatuan, kegiatanNama, jenis, catatan, catatanAtasan, lokasiKoordinat, lokasiAlamat;
    Date tanggal;
    List<String> file = new ArrayList<>();
    boolean checked = false;
    boolean isApproval8HariKebelakangTerkunci = true;

    public int getNomor() {
        return nomor;
    }

    public void setNomor(int nomor) {
        this.nomor = nomor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAktivitasId() {
        return aktivitasId;
    }

    public void setAktivitasId(int aktivitasId) {
        this.aktivitasId = aktivitasId;
    }

    public int getAktivitasBeban() {
        return aktivitasBeban;
    }

    public void setAktivitasBeban(int aktivitasBeban) {
        this.aktivitasBeban = aktivitasBeban;
    }

    public int getKegiatanId() {
        return kegiatanId;
    }

    public void setKegiatanId(int kegiatanId) {
        this.kegiatanId = kegiatanId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatusNama() {
        return statusNama;
    }

    public void setStatusNama(String statusNama) {
        this.statusNama = statusNama;
    }

    public String getAktivitasNama() {
        return aktivitasNama;
    }

    public void setAktivitasNama(String aktivitasNama) {
        this.aktivitasNama = aktivitasNama;
    }

    public String getAktivitasSatuan() {
        return aktivitasSatuan;
    }

    public void setAktivitasSatuan(String aktivitasSatuan) {
        this.aktivitasSatuan = aktivitasSatuan;
    }

    public String getKegiatanNama() {
        return kegiatanNama;
    }

    public void setKegiatanNama(String kegiatanNama) {
        this.kegiatanNama = kegiatanNama;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public String getCatatanAtasan() {
        return catatanAtasan;
    }

    public void setCatatanAtasan(String catatanAtasan) {
        this.catatanAtasan = catatanAtasan;
    }

    public String getLokasiKoordinat() {
        return lokasiKoordinat;
    }

    public void setLokasiKoordinat(String lokasiKoordinat) {
        this.lokasiKoordinat = lokasiKoordinat;
    }

    public String getLokasiAlamat() {
        return lokasiAlamat;
    }

    public void setLokasiAlamat(String lokasiAlamat) {
        this.lokasiAlamat = lokasiAlamat;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public List<String> getFile() {
        return file;
    }

    public void setFile(List<String> file) {
        this.file = file;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isApproval8HariKebelakangTerkunci() {
        return isApproval8HariKebelakangTerkunci;
    }

    public void setApproval8HariKebelakangTerkunci(boolean approval8HariKebelakangTerkunci) {
        isApproval8HariKebelakangTerkunci = approval8HariKebelakangTerkunci;
    }
}
