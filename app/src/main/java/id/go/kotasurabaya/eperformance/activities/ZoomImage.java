package id.go.kotasurabaya.eperformance.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.ablanco.zoomy.Zoomy;
import com.squareup.picasso.Picasso;
import id.go.kotasurabaya.eperformance.R;

public class ZoomImage extends Activity {

    ImageView image;
    String http = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom);

        Intent intent = getIntent();

        image = (ImageView) findViewById(R.id.pinchZoomImageView);
        http = intent.getStringExtra("url");

        Picasso.get().load(http).into(image);
        Zoomy.Builder builder = new Zoomy.Builder(this).target(image)
                .enableImmersiveMode(false)
                .animateZooming(false);
        builder.register();

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
