package id.go.kotasurabaya.eperformance.data;

import id.go.kotasurabaya.eperformance.activities.MainActivity;
import id.go.kotasurabaya.eperformance.activities.MainActivity2;
import id.go.kotasurabaya.eperformance.activities.SplashScreen;

/**
 * Created by enith on 10/3/2015.
 */
public final class St {
    private static St singleton;
    MainActivity main = null;
    MainActivity2 main2 = null;
    SplashScreen splashScreen = null;

    String phone, token, device_id;
    LoginModel login;
    LoginOsModel loginOs;

    String koplak;
    String aktif_bulan, aktif_bawahan_id, aktif_bawahan_nama;
    String aktif_status;
    String jml_per_page = "10";
    boolean bawahan_pns;

    private St(){
    }

    public static St getInstance() {
        if(singleton == null)
        {
            singleton = new St();
        }
        return singleton;
    }

    public MainActivity getMain() {
        return main;
    }
    public void setMain(MainActivity main) {
        this.main = main;
    }

    public MainActivity2 getMain2() {
        return main2;
    }

    public void setMain2(MainActivity2 main2) {
        this.main2 = main2;
    }

    public SplashScreen getSplashScreen() {
        return splashScreen;
    }

    public void setSplashScreen(SplashScreen splashScreen) {
        this.splashScreen = splashScreen;
    }

    public String getAktif_status() {
        return aktif_status;
    }

    public void setAktif_status(String aktif_status) {
        this.aktif_status = aktif_status;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }


    public LoginModel getLogin() {
        return login;
    }

    public void setLogin(LoginModel login) {
        this.login = login;
    }

    public LoginOsModel getLoginOs() {
        return loginOs;
    }

    public void setLoginOs(LoginOsModel loginOs) {
        this.loginOs = loginOs;
    }

    public String getKoplak() {
        return koplak;
    }

    public void setKoplak(String koplak) {
        this.koplak = koplak;
    }

    public String getAktif_bulan() {
        return aktif_bulan;
    }

    public void setAktif_bulan(String aktif_bulan) {
        this.aktif_bulan = aktif_bulan;
    }

    public String getAktif_bawahan_id() {
        return aktif_bawahan_id;
    }

    public void setAktif_bawahan_id(String aktif_bawahan_id) {
        this.aktif_bawahan_id = aktif_bawahan_id;
    }

    public String getAktif_bawahan_nama() {
        return aktif_bawahan_nama;
    }

    public void setAktif_bawahan_nama(String aktif_bawahan_nama) {
        this.aktif_bawahan_nama = aktif_bawahan_nama;
    }

    public String getJml_per_page() {
        return jml_per_page;
    }

    public void setJml_per_page(String jml_per_page) {
        this.jml_per_page = jml_per_page;
    }

    public boolean isBawahan_pns() {
        return bawahan_pns;
    }

    public void setBawahan_pns(boolean bawahan_pns) {
        this.bawahan_pns = bawahan_pns;
    }
}
