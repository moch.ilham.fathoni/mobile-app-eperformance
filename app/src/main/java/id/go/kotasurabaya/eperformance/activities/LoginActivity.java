package id.go.kotasurabaya.eperformance.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import id.go.kotasurabaya.eperformance.GPSTracker;
import id.go.kotasurabaya.eperformance.R;
import id.go.kotasurabaya.eperformance.api.APIService;
import id.go.kotasurabaya.eperformance.api.AuthAPI;
import id.go.kotasurabaya.eperformance.app.AppConst;
import id.go.kotasurabaya.eperformance.data.LoginModel;
import id.go.kotasurabaya.eperformance.data.LoginOsModel;
import id.go.kotasurabaya.eperformance.data.St;
import id.go.kotasurabaya.eperformance.helpers.M;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import id.go.kotasurabaya.eperformance.helpers.Sess;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;

public class LoginActivity extends AppCompatActivity implements OnClickListener {
    private Button loginBtn, registerBtn;
    private TextView sopBtn;
    private EditText usernameInput, passwordInput;
    private String username, password, phone, token, device_id;
    private ImageView avatar;
    private Intent mIntent;
    private RadioButton pns, os;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (M.getToken(this) != null) {
            Sess s = new Sess(getApplicationContext());
            String status = s.ambil("sts");
            if (status.equalsIgnoreCase("os")) {
                mIntent = new Intent(this, MainActivity2.class);
                startActivity(mIntent);
                finish();
            } else {
                mIntent = new Intent(this, MainActivity.class);
                startActivity(mIntent);
                finish();
            }
        }

        setContentView(R.layout.activity_login);
        initializeView();

        GPSTracker gps = new GPSTracker(this);
        if (!gps.canGetLocation()) {
            gps.showSettingsAlert();
        }
        /*
        {"data":{"success":1,"bawahan":[{"bawahanId":10009482,"nama":"ADONIKAM ATMOKO ADI, A.Md","nip":"198204092010011009","jabatan":"Teknis 1"},{"bawahanId":10000031,"nama":"DONI PRASETYANTO, SE","nip":"197906252010011002","jabatan":"Teknis 4"},{"bawahanId":10000020,"nama":"DRS. ATMO","nip":"196403232007011013","jabatan":"Teknis 3"},{"bawahanId":10008889,"nama":"IMAM MAHMUDI, ST","nip":"197908102010011014","jabatan":"Teknis 1"},{"bawahanId":10010407,"nama":"M. RR. EKKIE NOORISMA A., S.E.","nip":"198411042014022001","jabatan":"Teknis 3"},{"bawahanId":10000021,"nama":"MURNI, B.SC","nip":"196805301994012001","jabatan":"Teknis 5"},{"bawahanId":10000005,"nama":"R. ANDREAS HANAFIA, ST","nip":"197210252001121004","jabatan":"Teknis 1"},{"bawahanId":10000018,"nama":"ROFI I","nip":"196202231998031001","jabatan":"Operasional 1"},{"bawahanId":10000022,"nama":"TITIEN ANDRIANA, SE","nip":"196901241993022003","jabatan":"Teknis 5"}]}}
         */
        //robben - 198009132002121001
        //usernameInput.setText("198009132002121001");
        //passwordInput.setText("akupasswordnya");
        //ekkie - 198411042014022001
        //usernameInput.setText("198411042014022001");
        //passwordInput.setText("0101");
        //adon - 198204092010011009
        //usernameInput.setText("198204092010011009");
        //passwordInput.setText("0101");
        //uki - 3509050912850002
        //usernameInput.setText("3509050912850002");
        //passwordInput.setText("0101");
    }

    public void initializeView() {
        pns = (RadioButton) findViewById(R.id.rb_pns);
        os = (RadioButton) findViewById(R.id.rb_non_pns);
        sopBtn = (TextView) findViewById(R.id.sopBtn);
        loginBtn = (Button) findViewById(R.id.loginBtn);
        //registerBtn = (Button) findViewById(R.id.btnSubmit);
        usernameInput = (EditText) findViewById(R.id.username);
        passwordInput = (EditText) findViewById(R.id.password);
        avatar = (ImageView) findViewById(R.id.avatar);
        loginBtn.setOnClickListener(this);
        sopBtn.setOnClickListener(this);
        sopBtn.startAnimation(AnimationUtils.loadAnimation(LoginActivity.this, R.anim.move));
        //registerBtn.setOnClickListener(this);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setTitle(R.string.title_login);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.loginBtn) {
            username = usernameInput.getText().toString().trim();
            password = passwordInput.getText().toString().trim();
            token = St.getInstance().getToken();
            if (St.getInstance().getPhone() == null) {
                phone = "+123456789";
            } else if (St.getInstance().getPhone().equalsIgnoreCase("")) {
                phone = "+123456789";
            } else {
                phone = St.getInstance().getPhone();
            }
            device_id = St.getInstance().getDevice_id();
            Sess sess = new Sess(this);
            if (username.length() < 3) {
                usernameInput.setError("Invalid Username");
            } else if (password.length() < 1) {
                passwordInput.setError("Invalid Password");
            } else {
                if (pns.isChecked()) {
                    M.showLoadingDialog(this);
                    AuthAPI mAuthAPI = APIService.createService(AuthAPI.class, M.getToken(LoginActivity.this));
                    mAuthAPI.login("loginPegawai", username, password,
                            phone, device_id,
                            sess.ambil("reqid"),
                            token,
                            new Callback<LoginModel>() {
                                @Override
                                public void success(LoginModel loginModel, retrofit.client.Response response) {
                                    M.hideLoadingDialog();
                                    for (Header header : response.getHeaders()) {
                                        if (null != header.getName() && header.getName().equals("Set-Cookie")) {
                                            Log.d(AppConst.TAG, header.getName() + " = " + header.getValue());
                                            for (String possibleSessionCookie : header.getValue().split(";")) {
                                                if (possibleSessionCookie.startsWith("BAMBANGTRIBASUKI") && possibleSessionCookie.contains("=")) {
                                                    String session = possibleSessionCookie.split("=")[1];
                                                    Log.d(AppConst.TAG, "k=" + session);
                                                    St.getInstance().setKoplak(session);
                                                }
                                            }
                                        }
                                    }


                                    if (loginModel == null) {
                                        M.T(LoginActivity.this, "invalid respone");
                                        return;
                                    }
                                    if (loginModel.getData() == null) {
                                        M.T(LoginActivity.this, "invalid respone");
                                        return;
                                    }
                                    if (loginModel.getData().getSuccess() == 1) {
                                        M.setToken(loginModel.getData().getToken(), LoginActivity.this);
                                        M.setID(Integer.parseInt(loginModel.getData().getPegawaiId()), LoginActivity.this);
                                        St.getInstance().setLogin(loginModel);
                                        //
                                        //Log.d(this.getClass().getSimpleName(), "atasan="+loginModel.getData().getAtasan());
                                        //
                                        Sess s = new Sess(getApplicationContext());
                                        s.simpen("email", username);
                                        s.simpen("p", password);
                                        s.simpen("sts", "pns");
                                        mIntent = new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(mIntent);
                                        finish();
                                    } else {
                                        M.T(LoginActivity.this, loginModel.getData().getMsg());
                                    }

                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    M.hideLoadingDialog();
                                    M.T(LoginActivity.this, getString(R.string.ServerError));
                                    System.out.println("ERR: " + error.getMessage());
                                }
                            });
                } else if (os.isChecked()) {
                    M.showLoadingDialog(this);
                    AuthAPI mAuthAPI = APIService.createService(AuthAPI.class, M.getToken(LoginActivity.this));
                    mAuthAPI.loginOS("loginPegawai", username, password,
                            phone, device_id,
                            sess.ambil("reqid"),
                            token,
                            new Callback<LoginOsModel>() {
                                @Override
                                public void success(LoginOsModel loginModel, retrofit.client.Response response) {
                                    M.hideLoadingDialog();
                                    for (Header header : response.getHeaders()) {
                                        if (null != header.getName() && header.getName().equals("Set-Cookie")) {
                                            Log.d(AppConst.TAG, header.getName() + " = " + header.getValue());
                                            for (String possibleSessionCookie : header.getValue().split(";")) {
                                                if (possibleSessionCookie.startsWith("BAMBANGTRIBASUKI") && possibleSessionCookie.contains("=")) {
                                                    String session = possibleSessionCookie.split("=")[1];
                                                    Log.d(AppConst.TAG, "k=" + session);
                                                    St.getInstance().setKoplak(session);
                                                }
                                            }
                                        }
                                    }


                                    if (loginModel == null) {
                                        M.T(LoginActivity.this, "invalid respone");
                                        return;
                                    }
                                    if (loginModel.isSuccess()) {
                                        M.setToken(loginModel.getData().getToken(), LoginActivity.this);
                                        M.setID(loginModel.getData().getPegawai_id(), LoginActivity.this);
                                        St.getInstance().setLoginOs(loginModel);
                                        //
                                        //Log.d(this.getClass().getSimpleName(), "atasan="+loginModel.getData().getAtasan());
                                        //
                                        Sess s = new Sess(getApplicationContext());
                                        s.simpen("email", username);
                                        s.simpen("p", password);
                                        s.simpen("sts", "os");
                                        mIntent = new Intent(LoginActivity.this, MainActivity2.class);
                                        startActivity(mIntent);
                                        finish();
                                    } else {
                                        M.T(LoginActivity.this, loginModel.getMessage());
                                    }

                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    M.hideLoadingDialog();
                                    M.T(LoginActivity.this, getString(R.string.ServerError));
                                    Log.d("Error response login", error.getMessage());
                                    System.out.println("ERR: " + error.getMessage());
                                }
                            });
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                    alertDialog.setTitle("Peringatan!");
                    alertDialog.setMessage("Silahkan pilih status pegawai");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }
        } //else if (v.getId() == R.id.btnSubmit) {

        //}
        else if (v.getId() == R.id.sopBtn) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://eperformance.surabaya.go.id/2019/download2-route/ge82de45g7b4958c712896393/id"));
                startActivity(browserIntent);
            } catch (Exception e) {
                M.T(LoginActivity.this, e.getMessage());
            }
        }

    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Apakah Anda Ingin Keluar Aplikasi ?");
        alertDialogBuilder.setPositiveButton("Ya",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                        System.exit(0);
                    }
                });

        alertDialogBuilder.setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        //Showing the alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
