package id.go.kotasurabaya.eperformance.data;


public class LoginModel {
    SvrData data;

    public SvrData getData() {
        return data;
    }

    public void setData(SvrData data) {
        this.data = data;
    }


    public class SvrData {
        int success, sd;
        String msg, token;
        String pegawaiId, nama, nip;
        boolean isStaf;
        String jabatan, Atasan;
        int jmlLevel;
        int levelStruktural;
        boolean isStrukturalSelainKasieKelurahan;
        boolean isKapdAsistenSekda;
        boolean isKunciInputHariLalu;

        public boolean isKunciInputHariLalu() {
            return isKunciInputHariLalu;
        }

        public void setKunciInputHariLalu(boolean kunciInputHariLalu) {
            isKunciInputHariLalu = kunciInputHariLalu;
        }

        public boolean isCamat() {
            return isCamat;
        }

        public void setCamat(boolean camat) {
            isCamat = camat;
        }

        boolean isCamat;


        int totalNotifikasi;

        public int getTotalNotifikasi() {
            return totalNotifikasi;
        }

        public void setTotalNotifikasi(int totalNotifikasi) {
            this.totalNotifikasi = totalNotifikasi;
        }

        public int getSuccess() {
            return success;
        }

        public void setSuccess(int success) {
            this.success = success;
        }

        public int getSd() {
            return sd;
        }

        public void setSd(int sd) {
            this.sd = sd;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getPegawaiId() {
            return pegawaiId;
        }

        public void setPegawaiId(String pegawaiId) {
            this.pegawaiId = pegawaiId;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getNip() {
            return nip;
        }

        public void setNip(String nip) {
            this.nip = nip;
        }

        public boolean isStrukturalSelainKasieKelurahan() {
            return isStrukturalSelainKasieKelurahan;
        }

        public void setIsStrukturalSelainKasieKelurahan(boolean isStrukturalSelainKasieKelurahan) {
            this.isStrukturalSelainKasieKelurahan = isStrukturalSelainKasieKelurahan;
        }

        public boolean isKapdAsistenSekda() {
            return isKapdAsistenSekda;
        }

        public void setKapdAsistenSekda(boolean kapdAsistenSekda) {
            isKapdAsistenSekda = kapdAsistenSekda;
        }

        public boolean isStaf() {
            return isStaf;
        }

        public void setIsStaf(boolean isStaf) {
            this.isStaf = isStaf;
        }

        public String getJabatan() {
            return jabatan;
        }

        public void setJabatan(String jabatan) {
            this.jabatan = jabatan;
        }

        public String getAtasan() {
            return Atasan;
        }

        public void setAtasan(String atasan) {
            Atasan = atasan;
        }

        public int getJmlLevel() {
            return jmlLevel;
        }

        public void setJmlLevel(int jmlLevel) {
            this.jmlLevel = jmlLevel;
        }

        public int getLevelStruktural() {
            return levelStruktural;
        }

        public void setLevelStruktural(int levelStruktural) {
            this.levelStruktural = levelStruktural;
        }

    }

}
