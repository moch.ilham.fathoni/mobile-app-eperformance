package id.go.kotasurabaya.eperformance.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.go.kotasurabaya.eperformance.R;
import id.go.kotasurabaya.eperformance.animation.AnimUtils;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HomeListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 2;
    private static final int TYPE_ITEM = 1;
    private Activity mActivity;
    private LayoutInflater mInflater;
    private Intent mIntent;
    private int mPreviousPosition = 0;


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {

            View view = mInflater.inflate(R.layout.home_header, parent, false);
            HeaderViewHolder holder = new HeaderViewHolder(view);
            return holder;
        } else {
            View view = mInflater.inflate(R.layout.bawahan_aktivitas_list_item, parent, false);
            ViewHolderPosts holder = new ViewHolderPosts(view);
            return holder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ViewHolderPosts) {

        } else if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
        }

        if (position > mPreviousPosition) {
            AnimUtils.animate(holder, true);
        } else {
            AnimUtils.animate(holder, false);
        }
        mPreviousPosition = position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<int[]>();

        Pattern pattern = Pattern.compile(prefix + "\\w+");
        Matcher matcher = pattern.matcher(body);

        // Check all occurrences
        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    public class ViewHolderPosts extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name,
                timestamp,
                statusMsg,
                komentarNum,
                viewsNum,
                postNewStatus,
                linkTitlePreview,
                youtubeTitlePreview,
                linkUrlPreview,
                youtubeUrlPreview,
                placeValuePreview;
        ImageView profilePic,
                feedImageView,
                linkImagePreview,
                youtubeImagePreview;
        ImageButton shareBtn, likeBtn, postOptions, postSaveBtn;
        LinearLayout editPostSection;
        FrameLayout linkPreview, youtubePreview;
        LinearLayout placePreviewLayout;
        //
        LinearLayout postFooter;


        public ViewHolderPosts(View itemView) {
            super(itemView);

        }

        @Override
        public void onClick(final View v) {
            final int postPosition = this.getPosition() - 1;
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public AdView mAdView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            mAdView = (AdView) itemView.findViewById(R.id.adView);
            LinearLayout ll = (LinearLayout) itemView.findViewById(R.id.contentHead);
            ll.setVisibility(View.GONE);



        }
    }
}
