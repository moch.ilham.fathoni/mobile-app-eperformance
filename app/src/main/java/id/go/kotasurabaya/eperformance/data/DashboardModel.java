package id.go.kotasurabaya.eperformance.data;

/**
 * Created by enith on 10/4/2015.
 */
public class DashboardModel {
    int id;
    String quota;
    String used;
    String available;
    String new_request_cnt;
    String livereview_jabatan_avg;
    String livereview_avg;
    String livereview_cnt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuota() {
        return quota;
    }

    public void setQuota(String quota) {
        this.quota = quota;
    }

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getNew_request_cnt() {
        return new_request_cnt;
    }

    public void setNew_request_cnt(String new_request_cnt) {
        this.new_request_cnt = new_request_cnt;
    }


    public String getLivereview_avg() {
        return livereview_avg;
    }

    public void setLivereview_avg(String livereview_avg) {
        this.livereview_avg = livereview_avg;
    }

    public String getLivereview_cnt() {
        return livereview_cnt;
    }

    public void setLivereview_cnt(String livereview_cnt) {
        this.livereview_cnt = livereview_cnt;
    }



    public String getLivereview_jabatan_avg() {
        return livereview_jabatan_avg;
    }

    public void setLivereview_jabatan_avg(String livereview_jabatan_avg) {
        this.livereview_jabatan_avg = livereview_jabatan_avg;
    }

}
