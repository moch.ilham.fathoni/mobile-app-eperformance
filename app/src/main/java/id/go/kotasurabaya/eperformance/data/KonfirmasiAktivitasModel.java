package id.go.kotasurabaya.eperformance.data;


import com.google.gson.annotations.SerializedName;

public class KonfirmasiAktivitasModel {
    @SerializedName("aktivitasPegawaiId")
    String aktivitasPegawaiId;
    @SerializedName("catatanAtasan")
    String catatanAtasan;

    public String getAktivitasPegawaiId() {
        return aktivitasPegawaiId;
    }

    public void setAktivitasPegawaiId(String aktivitasPegawaiId) {
        this.aktivitasPegawaiId = aktivitasPegawaiId;
    }

    public String getCatatanAtasan() {
        return catatanAtasan;
    }

    public void setCatatanAtasan(String catatanAtasan) {
        this.catatanAtasan = catatanAtasan;
    }
}
